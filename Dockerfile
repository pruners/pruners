FROM registry.gitlab.inria.fr/inria-ci/docker/rust:1.72
# Create a blank project with the same Cargo.toml to install dependencies
# This trick documented here:
# https://stackoverflow.com/questions/42130132/can-cargo-download-and-build-dependencies-without-also-building-the-application
RUN rustc --version
RUN cargo install cargo2junit
RUN cargo init --name pruners
COPY Cargo.toml .
RUN cargo build && cargo build --release
RUN rm -r Cargo.toml src
RUN rustup component add rustfmt
