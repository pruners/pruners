/**
This file is part of Pruners.

Pruners is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or any later version.

Pruners is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Pruners. If not, see <https://www.gnu.org/licenses/>.

Copyright (C) 2023, Inria

Contacts:
Thierry Martinez <thierry.martinez@inria.fr>
Simon Legrand <simon.legrand@inria.fr>
*/
use clap::Parser;

lalrpop_mod!(pub prune);
use crate::{ast, error, loc};

#[derive(Parser)]
#[command(author, version, about = "Parameter study automation tool")]
pub struct Cli {
    /// Script to execute
    pub script: std::path::PathBuf,
    /// Command to execute. Equivalent to an `execute` call at the end of the script
    #[arg(short, long)]
    pub exec: Option<String>,
    /// Print combinations defined in the script
    #[arg(short, long)]
    pub print_combinations: bool,
    /// Skip branches where condition is true (conflicts with keep option)
    #[arg(short, long)]
    pub skip: Option<String>,
    /// Keep branches where condition is true (conflicts with skip option)
    #[arg(short, long)]
    pub keep: Option<String>,
    /// Parallel jobs
    #[arg(short, long)]
    pub jobs: Option<usize>,
}

pub fn parse_condition(s: &str) -> Result<ast::Value, error::Error<loc::Offset>> {
    let open_cond = prune::ValueParser::new()
        .parse(s)
        .map_err(|error| error::Error::from_parse_error(error).map_location(loc::Offset::Offset));

    open_cond.and_then(|oc| Ok(oc.desugar()?))
}
