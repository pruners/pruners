/**
This file is part of Pruners.

Pruners is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or any later version.

Pruners is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Pruners. If not, see <https://www.gnu.org/licenses/>.

Copyright (C) 2023, Inria

Contacts:
Thierry Martinez <thierry.martinez@inria.fr>
Simon Legrand <simon.legrand@inria.fr>
*/
use crate::{error, loc};
use indexmap;
use regex;
use serde::{Deserialize, Serialize};

pub fn quote_string(s: &str) -> String {
    let mut result = String::new();
    result.push('\"');
    for c in s.chars() {
        if c == '"' || c == '\\' {
            result.push('\\');
        }
        result.push(c);
    }
    result.push('\"');
    result
}

/** Remove '"' or "'" around the string and preserve '\' symbols only
if they are not in front of quotes ('"' or "'") or other '\' */
pub fn unquote_string(s: &str) -> Option<String> {
    let mut result = String::new();
    let mut iter = s.chars();
    let quote = iter.next()?;
    let mut escaped = false;
    for c in iter {
        if escaped {
            escaped = false;
            if c != '"' && c != '\'' && c != '\\' {
                /* requiring quotes to be escaped makes unquote_string
                 * onto (surjective) */
                result.push('\\');
            }
        } else if c == quote {
            break;
        } else if c == '\\' {
            escaped = true;
            continue;
        }
        result.push(c)
    }
    Some(result)
}

#[cfg(test)]
mod tests {
    use crate::ast;

    #[test]
    fn test_unquote_string() -> () {
        assert_eq!(ast::unquote_string(r"'blabla'"), Some(r"blabla".into()));
        assert_eq!(ast::unquote_string(r"'blabla\n'"), Some(r"blabla\n".into()));
        assert_eq!(
            ast::unquote_string("'\\\'\\\"'"), /* `\'\"` -> `'"` */
            Some("'\"".into())
        );
        assert_eq!(
            ast::unquote_string("'\\\'\"'"), /* `\'"` -> `'"` */
            Some("'\"".into())
        );
        assert_eq!(ast::unquote_string(r"'blabla\''"), Some(r"blabla'".into()));
        assert_eq!(
            ast::unquote_string(r"'blabla\\\''"),
            Some(r"blabla\'".into())
        );
        assert_eq!(
            ast::unquote_string(r"'blabla\\\\\''"),
            Some(r"blabla\\'".into())
        );
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct Var(pub std::sync::Arc<String>);

impl From<&str> for Var {
    fn from(s: &str) -> Self {
        Var(std::sync::Arc::new(String::from(s)))
    }
}

impl std::fmt::Display for Var {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(PartialEq, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Constant {
    Unit,
    Bool(bool),
    Int(i64),
    Float(f64),
    String(String),
    Type(Type),
    List(Vec<ConstantRef>),
    Dict(indexmap::IndexMap<String, ConstantRef>),
}

pub fn format_dict(
    fmt: &mut std::fmt::Formatter,
    dict: &indexmap::IndexMap<std::string::String, ConstantRef>,
) -> std::fmt::Result {
    write!(fmt, "{{")?;
    let mut iter = dict.iter();
    match iter.next() {
        None => (),
        Some((key, value)) => {
            write!(fmt, "{}:{}", key, value)?;
            for (key, value) in iter {
                write!(fmt, ", {}:{}", key, value)?;
            }
        }
    }
    write!(fmt, "}}")
}

impl std::fmt::Display for Constant {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Constant::Unit => write!(fmt, "()")?,
            Constant::Bool(b) => write!(fmt, "{}", b)?,
            Constant::Int(i) => write!(fmt, "{}", i)?,
            Constant::Float(f) => write!(fmt, "{}", f)?,
            Constant::String(s) => write!(fmt, "{}", quote_string(s))?,
            Constant::Type(t) => write!(fmt, "{}", t)?,
            Constant::List(l) => {
                write!(fmt, "[")?;
                let mut iter = l.iter();
                match iter.next() {
                    None => (),
                    Some(value) => {
                        write!(fmt, "{}", value)?;
                        for value in iter {
                            write!(fmt, ", {}", value)?;
                        }
                    }
                }
                write!(fmt, "]")?;
            }
            Constant::Dict(d) => format_dict(fmt, d)?,
        }
        Ok(())
    }
}

#[derive(Clone, PartialEq, Deserialize, Serialize, Debug)]
pub struct ConstantRef(pub std::sync::Arc<Constant>);

impl std::fmt::Display for ConstantRef {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<bool> for ConstantRef {
    fn from(b: bool) -> Self {
        ConstantRef(std::sync::Arc::new(Constant::Bool(b)))
    }
}

impl From<i64> for ConstantRef {
    fn from(i: i64) -> Self {
        ConstantRef(std::sync::Arc::new(Constant::Int(i)))
    }
}

impl From<f64> for ConstantRef {
    fn from(f: f64) -> Self {
        ConstantRef(std::sync::Arc::new(Constant::Float(f)))
    }
}

impl From<&str> for ConstantRef {
    fn from(s: &str) -> Self {
        ConstantRef(std::sync::Arc::new(Constant::String(String::from(s))))
    }
}

impl From<String> for ConstantRef {
    fn from(s: String) -> Self {
        ConstantRef(std::sync::Arc::new(Constant::String(s)))
    }
}

impl From<Type> for ConstantRef {
    fn from(t: Type) -> Self {
        ConstantRef(std::sync::Arc::new(Constant::Type(t)))
    }
}

impl<Item> From<Vec<Item>> for ConstantRef
where
    ConstantRef: From<Item>,
{
    fn from(v: Vec<Item>) -> Self {
        ConstantRef(std::sync::Arc::new(Constant::List(
            v.into_iter().map(ConstantRef::from).collect(),
        )))
    }
}

impl From<indexmap::IndexMap<String, ConstantRef>> for ConstantRef {
    fn from(v: indexmap::IndexMap<String, ConstantRef>) -> Self {
        ConstantRef(std::sync::Arc::new(Constant::Dict(v)))
    }
}
impl ConstantRef {
    pub fn unit() -> Self {
        ConstantRef(std::sync::Arc::new(Constant::Unit))
    }

    #[allow(dead_code)]
    pub fn empty_list() -> Self {
        ConstantRef(std::sync::Arc::new(Constant::List(vec![])))
    }

    pub fn as_bool(&self) -> std::result::Result<bool, error::Located> {
        match **self {
            Constant::Bool(b) => Ok(b),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::Bool,
                constant: self.clone(),
            }),
        }
    }

    pub fn as_int(&self) -> std::result::Result<i64, error::Located> {
        match **self {
            Constant::Int(i) => Ok(i),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::Int,
                constant: self.clone(),
            }),
        }
    }

    pub fn as_float(&self) -> std::result::Result<f64, error::Located> {
        match **self {
            Constant::Int(i) => Ok(i as f64),
            Constant::Float(f) => Ok(f),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::Float,
                constant: self.clone(),
            }),
        }
    }

    pub fn as_string(&self) -> std::result::Result<&String, error::Located> {
        match &**self {
            Constant::String(s) => Ok(s),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::String,
                constant: self.clone(),
            }),
        }
    }

    pub fn any_as_string(&self) -> String {
        match &**self {
            Constant::String(s) => s.clone(),
            _ => format!("{}", self),
        }
    }

    pub fn as_list(&self) -> std::result::Result<&Vec<ConstantRef>, error::Located> {
        match &**self {
            Constant::List(l) => Ok(l),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::List,
                constant: self.clone(),
            }),
        }
    }
    pub fn as_dict(
        &self,
    ) -> std::result::Result<&indexmap::IndexMap<String, ConstantRef>, error::Located> {
        match &**self {
            Constant::Dict(d) => Ok(d),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::Dict,
                constant: self.clone(),
            }),
        }
    }
}

pub trait TypedConversion: Sized {
    fn get_type() -> Type;

    fn try_from(value: &ConstantRef) -> std::result::Result<Self, error::Located>;
}

pub trait TypedConversionRef: Sized {
    fn get_type() -> Type;

    fn try_from(value: &ConstantRef) -> std::result::Result<&Self, error::Located>;
}

impl TypedConversion for bool {
    fn get_type() -> Type {
        Type::Bool
    }

    fn try_from(value: &ConstantRef) -> std::result::Result<bool, error::Located> {
        match &**value {
            Constant::Bool(b) => Ok(*b),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::Bool,
                constant: value.clone(),
            }),
        }
    }
}

impl TypedConversion for i64 {
    fn get_type() -> Type {
        Type::Int
    }

    fn try_from(value: &ConstantRef) -> std::result::Result<i64, error::Located> {
        match &**value {
            Constant::Int(i) => Ok(*i),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::Int,
                constant: value.clone(),
            }),
        }
    }
}

impl TypedConversion for f64 {
    fn get_type() -> Type {
        Type::Float
    }

    fn try_from(value: &ConstantRef) -> std::result::Result<f64, error::Located> {
        match &**value {
            Constant::Int(i) => Ok(*i as f64),
            Constant::Float(f) => Ok(*f),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::Float,
                constant: value.clone(),
            }),
        }
    }
}

impl TypedConversionRef for i64 {
    fn get_type() -> Type {
        Type::Int
    }

    fn try_from(value: &ConstantRef) -> std::result::Result<&i64, error::Located> {
        match &**value {
            Constant::Int(i) => Ok(i),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::String,
                constant: value.clone(),
            }),
        }
    }
}

impl TypedConversionRef for String {
    fn get_type() -> Type {
        Type::String
    }

    fn try_from(value: &ConstantRef) -> std::result::Result<&String, error::Located> {
        match &**value {
            Constant::String(s) => Ok(s),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::String,
                constant: value.clone(),
            }),
        }
    }
}

impl TypedConversionRef for Vec<ConstantRef> {
    fn get_type() -> Type {
        Type::List
    }

    fn try_from(value: &ConstantRef) -> std::result::Result<&Vec<ConstantRef>, error::Located> {
        match &**value {
            Constant::List(l) => Ok(l),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::List,
                constant: value.clone(),
            }),
        }
    }
}

impl TypedConversionRef for indexmap::IndexMap<String, ConstantRef> {
    fn get_type() -> Type {
        Type::Dict
    }

    fn try_from(
        value: &ConstantRef,
    ) -> std::result::Result<&indexmap::IndexMap<String, ConstantRef>, error::Located> {
        match &**value {
            Constant::Dict(d) => Ok(d),
            _ => Err(error::Located::TypeMismatch {
                expected: Type::Dict,
                constant: value.clone(),
            }),
        }
    }
}

impl TypedConversionRef for ConstantRef {
    fn get_type() -> Type {
        Type::Any
    }

    fn try_from(value: &ConstantRef) -> std::result::Result<&ConstantRef, error::Located> {
        Ok(value)
    }
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub enum Type {
    Any,
    Unit,
    Bool,
    Int,
    Float,
    String,
    Type,
    List,
    Dict,
    Empty,
}

impl Type {
    pub fn is_subtype(&self, t: &Type) -> bool {
        match (self, t) {
            (_, Type::Any) => true,
            (Type::Unit, Type::Unit) => true,
            (Type::Bool, Type::Bool) => true,
            (Type::Int, Type::Int) => true,
            (Type::Int, Type::Float) => true,
            (Type::Float, Type::Float) => true,
            (Type::String, Type::String) => true,
            (Type::List, Type::List) => true,
            (Type::Dict, Type::Dict) => true,
            (Type::Type, Type::Type) => true,
            (Type::Empty, _) => true,
            _ => false,
        }
    }

    pub fn least_upperbound(&self, t: &Type) -> Type {
        match (self, t) {
            (Type::Bool, Type::Bool) => Type::Bool,
            (Type::Int, Type::Int) => Type::Int,
            (Type::Int, Type::Float) => Type::Float,
            (Type::Float, Type::Int) => Type::Float,
            (Type::Float, Type::Float) => Type::Float,
            (Type::String, Type::String) => Type::String,
            (Type::List, Type::List) => Type::List,
            (Type::Dict, Type::Dict) => Type::Dict,
            (Type::Type, Type::Type) => Type::Type,
            (Type::Empty, _) => t.clone(),
            (_, Type::Empty) => self.clone(),
            _ => Type::Any,
        }
    }
}

impl std::fmt::Display for Type {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Type::Any => write!(f, "_"),
            Type::Unit => write!(f, "()"),
            Type::Bool => write!(f, "bool"),
            Type::Int => write!(f, "int"),
            Type::Float => write!(f, "float"),
            Type::String => write!(f, "string"),
            Type::Type => write!(f, "type"),
            Type::List => write!(f, "list"),
            Type::Dict => write!(f, "dict"),
            Type::Empty => write!(f, "empty"),
        }
    }
}

impl From<&Constant> for Type {
    fn from(c: &Constant) -> Self {
        match c {
            Constant::Unit => Type::Unit,
            Constant::Bool(_) => Type::Bool,
            Constant::Int(_) => Type::Int,
            Constant::Float(_) => Type::Float,
            Constant::String(_) => Type::String,
            Constant::Type(_) => Type::Type,
            Constant::List(_) => Type::List,
            Constant::Dict(_) => Type::Dict,
        }
    }
}

impl std::ops::Deref for ConstantRef {
    type Target = Constant;

    fn deref(&self) -> &Self::Target {
        return &*self.0;
    }
}

/* Values parsed by the parser are represented by `FrontValue`
and these values are desugared into `Value` by `FrontValue::desugar`.
`FrontValue` has representation closer to the concrete syntax for
ranges, with ellipsis, whereas `Value` ranges have bounds and possibly
a context (the `f(.)` part in `[f(a) .. f(b)]`, represented by `Context`).

`FrontValue`, `Value` and `Context` are specializations of the type
`OpenValue` parameterized by `T`: `OpenValue` has a variant `Open(T)`,
making the type extensible. */
pub enum OpenValue<T> {
    Var(loc::Located<usize, Var>),
    Constant(ConstantRef),
    List(Vec<OpenValue<T>>),
    Dict(indexmap::IndexMap<String, OpenValue<T>>),
    Apply(loc::Located<usize, OpenApply<OpenValue<T>>>),
    MatchRegex(loc::Located<usize, (Box<OpenValue<T>>, regex::Regex)>),
    If(Box<loc::Located<usize, IfValue<T>>>),
    Open(T),
}

pub struct OpenApply<Value> {
    pub f: String,
    pub args: Vec<Value>,
}

impl OpenApply<Context> {
    pub fn fill<Open, I>(&self, values: &mut I) -> OpenApply<OpenValue<Open>>
    where
        I: Iterator<Item = OpenValue<Open>>,
    {
        OpenApply {
            f: self.f.to_string(),
            args: self.args.iter().map(|u| u.fill(values)).collect(),
        }
    }
}

pub struct IfValue<T> {
    pub cond: OpenValue<T>,
    pub then_branch: OpenValue<T>,
    pub else_branch: OpenValue<T>,
}

pub type FrontValue = OpenValue<Front>;

pub type FrontApply = OpenApply<FrontValue>;

pub type Value = OpenValue<Desugared>;

pub type Apply = OpenApply<Value>;

/* `Front` values have two variants for ellipsis:

   - `Range` represents the expression `a .. b`
     (where `..` is used as a binary operator) and

   - `RangeMark` is just an isolated `..` (where `..` is used in place
     of a value, as in `[a, .., b]`).
*/

pub enum Front {
    Range(loc::Located<usize, Box<(FrontValue, FrontValue)>>),
    RangeMark(loc::Located<usize, ()>),
}

/* `Desugared` values have two representations for ranges:

   - `Range` represents range constructions such as `[a .. b]`, and

   - `RangeApply` represents a binary function application with ranges
     such as `a + .. + b`.

   Both relies on the same type `Range` to represent the bounds,
   the possible second value (as in `[a, b, .., c]`), and
   the context (the `f(.)` part in `[f(a) .. f(b)]`).
*/

pub enum Desugared {
    Range(loc::Located<usize, Box<Range>>),
    RangeApply {
        f: String,
        initial_value: Option<Box<Value>>,
        range: loc::Located<usize, Box<Range>>,
        final_value: Option<Box<Value>>,
    },
}

pub struct Range {
    pub context: std::sync::Arc<Context>,
    pub begin: Value,
    pub second: Option<Value>,
    pub end: Value,
}

/* Context are values with holes, holes are represented with the
variant `Open(())`. */

pub type Context = OpenValue<()>;

#[derive(PartialEq, Eq)]
pub enum ZeroOrOne {
    Zero,
    One,
}

impl Context {
    /* `Context::new_zero_or_one(a, b)` computes the common
       zero-or-one-hole context  between `a` and `b`.

       If `a` and `b` are identical, there is no hole, and
       `Context::new` returns `(c, Zero)`, where the context `c` is
       just a copy of `a` (or `b`).

       If `a` and `b` differ, `Context::new_zero_or_one` returns
       `(c, One)` where `c` is the biggest one-hole context common to
       `a` and `b`.
    */

    fn new_hole() -> (Self, ZeroOrOne) {
        return (Context::Open(()), ZeroOrOne::One);
    }

    pub fn new_zero_or_one<A, B>(a: &OpenValue<A>, b: &OpenValue<B>) -> (Self, ZeroOrOne) {
        match (a, b) {
            (OpenValue::Var(a0), OpenValue::Var(b0)) if a0.v == b0.v => {
                (Context::Var(a0.clone()), ZeroOrOne::Zero)
            }
            (OpenValue::Constant(a0), OpenValue::Constant(b0)) if a0 == b0 => {
                (Context::Constant(a0.clone()), ZeroOrOne::Zero)
            }
            (OpenValue::List(a0), OpenValue::List(b0)) if a0.len() == b0.len() => {
                match Context::new_zero_or_one_vec(&mut a0.iter(), &mut b0.iter()) {
                    None => Context::new_hole(),
                    Some((contexts, zero_or_one)) => (Context::List(contexts), zero_or_one),
                }
            }
            (OpenValue::Dict(a0), OpenValue::Dict(b0)) => {
                let mut a0: Vec<(&String, &OpenValue<A>)> = a0.iter().collect();
                let mut b0: Vec<(&String, &OpenValue<B>)> = b0.iter().collect();
                a0.sort_by_key(|(k, _v)| *k);
                b0.sort_by_key(|(k, _v)| *k);
                if a0
                    .iter()
                    .zip(b0.iter())
                    .all(|((ka, _va), (kb, _vb))| ka == kb)
                {
                    let (keys, a0): (Vec<_>, Vec<_>) = a0.into_iter().unzip();
                    match Context::new_zero_or_one_vec(
                        &mut a0.into_iter(),
                        &mut b0.into_iter().map(|(_k, v)| v),
                    ) {
                        None => Context::new_hole(),
                        Some((contexts, zero_or_one)) => (
                            Context::Dict(
                                keys.into_iter()
                                    .zip(contexts.into_iter())
                                    .map(|(key, context)| (key.to_string(), context))
                                    .collect(),
                            ),
                            zero_or_one,
                        ),
                    }
                } else {
                    Context::new_hole()
                }
            }
            (OpenValue::Apply(a0), OpenValue::Apply(b0)) => {
                if a0.v.f == b0.v.f && a0.v.args.len() == b0.v.args.len() {
                    match Context::new_zero_or_one_vec(&mut a0.v.args.iter(), &mut b0.v.args.iter())
                    {
                        None => Context::new_hole(),
                        Some((contexts, zero_or_one)) => (
                            Context::Apply(loc::Located {
                                range: a0.range,
                                v: OpenApply {
                                    f: a0.v.f.to_string(),
                                    args: contexts,
                                },
                            }),
                            zero_or_one,
                        ),
                    }
                } else {
                    Context::new_hole()
                }
            }
            _ => Context::new_hole(),
        }
    }

    fn new_zero_or_one_vec<'a, A: 'a, B: 'a, I, J>(
        a: &mut I,
        b: &mut J,
    ) -> Option<(Vec<Context>, ZeroOrOne)>
    where
        I: Iterator<Item = &'a OpenValue<A>>,
        J: Iterator<Item = &'a OpenValue<B>>,
    {
        let (contexts, zero_or_ones): (Vec<_>, Vec<_>) = a
            .zip(b)
            .map(|(a, b)| Context::new_zero_or_one(a, b))
            .unzip();
        let mut ones = zero_or_ones
            .into_iter()
            .filter(|zero_or_one| *zero_or_one == ZeroOrOne::One);
        if ones.next().is_none() {
            Some((contexts, ZeroOrOne::Zero))
        } else if ones.next().is_none() {
            Some((contexts, ZeroOrOne::One))
        } else {
            None
        }
    }

    /* `Context::extract_one_hole(c, v)` returns a sub-term of `v`
    that is in the position of an hole of `c`, if any.
    This function supposes that `v` is an instance of `c`.
    The result is unspecified otherwise. */

    pub fn extract<T>(c: &Self, v: OpenValue<T>) -> Option<OpenValue<T>> {
        match (c, v) {
            (Context::Open(()), v) => Some(v),
            (Context::List(c), OpenValue::List(v)) => c
                .iter()
                .zip(v.into_iter())
                .find_map(|(c, v)| Context::extract(c, v)),
            (Context::Dict(c), OpenValue::Dict(v)) => {
                let mut c: Vec<_> = c.iter().collect();
                let mut v: Vec<_> = v.into_iter().collect();
                c.sort_by_key(|(k, _v)| k.to_string());
                v.sort_by_key(|(k, _v)| k.to_string());
                c.iter()
                    .zip(v.into_iter())
                    .find_map(|((_kc, c), (_kv, v))| Context::extract(c, v))
            }
            (Context::Apply(c), OpenValue::Apply(v)) => {
                c.v.args
                    .iter()
                    .zip(v.v.args.into_iter())
                    .find_map(|(c, v)| Context::extract(c, v))
            }
            _ => None,
        }
    }

    /* `Context::fill` fills the hole of a context, taking values from
    an iterator. */

    pub fn fill<Open, I>(&self, values: &mut I) -> OpenValue<Open>
    where
        I: Iterator<Item = OpenValue<Open>>,
    {
        match self {
            Context::Var(v) => OpenValue::Var(v.clone()),
            Context::Constant(c) => OpenValue::Constant(c.clone()),
            Context::List(l) => OpenValue::List(l.into_iter().map(|u| u.fill(values)).collect()),
            Context::Dict(d) => OpenValue::Dict(
                d.into_iter()
                    .map(|(k, u)| (k.to_string(), u.fill(values)))
                    .collect(),
            ),
            Context::Apply(app) => OpenValue::Apply(loc::Located {
                range: app.range,
                v: app.v.fill(values),
            }),
            Context::MatchRegex(m) => {
                let v = m.v.0.fill(values);
                OpenValue::MatchRegex(loc::Located {
                    range: m.range,
                    v: (Box::new(v), m.v.1.clone()),
                })
            }
            Context::If(_) => panic!("no if in context"),
            Context::Open(()) => values.next().unwrap(),
        }
    }
}

impl<Open> OpenValue<Open> {
    pub fn to_var(&self) -> std::result::Result<Var, error::Located> {
        match self {
            OpenValue::Var(x) => Ok(x.v.clone()),
            _ => Err(error::Located::VariableExpected),
        }
    }
}

pub struct Assignment<Value> {
    pub x: Var,
    pub value: Value,
}

pub struct If<Value, SubRules> {
    pub condition: Value,
    pub then: SubRules,
}

pub enum SimpleRule<Value> {
    Assign(Assignment<Value>),
    In(Assignment<Value>),
    Include(Value),
    Apply(loc::Located<usize, OpenApply<Value>>),
}

impl Assignment<FrontValue> {
    pub fn desugar(self) -> Result<Assignment<Value>> {
        Ok(Assignment {
            x: self.x.clone(),
            value: self.value.desugar()?,
        })
    }
}

impl FrontApply {
    pub fn desugar(self) -> Result<Apply> {
        Ok(Apply {
            f: self.f.clone(),
            args: self
                .args
                .into_iter()
                .map(FrontValue::desugar)
                .collect::<Result<Vec<Value>>>()?,
        })
    }
}

impl SimpleRule<FrontValue> {
    pub fn desugar(self) -> Result<SimpleRule<Value>> {
        Ok(match self {
            SimpleRule::Assign(assign) => SimpleRule::Assign(assign.desugar()?),
            SimpleRule::In(assign) => SimpleRule::In(assign.desugar()?),
            SimpleRule::Include(v) => SimpleRule::Include(v.desugar()?),
            SimpleRule::Apply(apply) => SimpleRule::Apply(loc::Located {
                range: apply.range,
                v: apply.v.desugar()?,
            }),
        })
    }
}

pub struct Collect<SubRules> {
    pub x: Var,
    pub rules: SubRules,
}

pub enum Rule<Value, SubRules> {
    Simple(SimpleRule<Value>),
    Linked(SubRules),
    If(If<Value, SubRules>),
    Collect(Collect<SubRules>),
}

pub struct Line {
    pub level: String,
    pub rule: Rule<FrontValue, ()>,
}

pub struct RuleTree(pub Vec<loc::Located<loc::Offset, Rule<Value, RuleTree>>>);

pub type Error = error::Error<loc::Offset>;

pub type Result<X> = std::result::Result<X, Error>;

impl FrontValue {
    pub fn desugar_range(
        range: loc::Range<usize>,
        begin: FrontValue,
        second: Option<FrontValue>,
        end: FrontValue,
    ) -> Result<Value> {
        let begin = begin.desugar()?;
        let end = end.desugar()?;
        let (context, zero_or_one) = Context::new_zero_or_one(&begin, &end);
        if zero_or_one == ZeroOrOne::Zero {
            Err(error::loc(range)(error::Located::EmptyRangeUnallowed))?
        }
        let begin = Context::extract(&context, begin).unwrap();
        let end = Context::extract(&context, end).unwrap();
        let (context, second) = match second {
            None => (context, None),
            Some(second) => {
                let second = second.desugar()?;
                let (context0, _zero_or_one) = Context::new_zero_or_one(&context, &second);
                /* context of context should have an hole */
                let residual = Context::extract(&context0, context).unwrap();
                match residual {
                    Context::Open(()) => (),
                    _ => Err(error::loc(range)(error::Located::EmptyRangeUnallowed))?,
                }
                let second = Context::extract(&context0, second);
                (context0, second)
            }
        };
        Ok(Value::Open(Desugared::Range(loc::Located {
            range,
            v: Box::new(Range {
                context: std::sync::Arc::new(context),
                begin,
                second,
                end,
            }),
        })))
    }

    fn desugar_list(mut l: Vec<FrontValue>) -> Result<Value> {
        Ok(
            match match l.len() {
                1 =>
                /* [a .. b] */
                {
                    match l.remove(0) {
                        FrontValue::Open(Front::Range(range)) => Some(FrontValue::desugar_range(
                            range.range,
                            range.v.0,
                            None,
                            range.v.1,
                        )?),
                        v => {
                            l.push(v);
                            None
                        }
                    }
                }
                2 =>
                /* [a, b .. c] */
                {
                    match l.remove(1) {
                        FrontValue::Open(Front::Range(range)) => Some(FrontValue::desugar_range(
                            range.range,
                            l.remove(0),
                            Some(range.v.0),
                            range.v.1,
                        )?),
                        v => {
                            l.insert(1, v);
                            None
                        }
                    }
                }
                3 =>
                /* [a, .., b] */
                {
                    match l.remove(1) {
                        FrontValue::Open(Front::RangeMark(range)) => {
                            let begin = l.remove(0);
                            let end = l.remove(0);
                            Some(FrontValue::desugar_range(range.range, begin, None, end)?)
                        }
                        v => {
                            l.insert(1, v);
                            None
                        }
                    }
                }
                4 =>
                /* [a, b, .., c] */
                {
                    match l.remove(2) {
                        FrontValue::Open(Front::RangeMark(range)) => {
                            let begin = l.remove(0);
                            let second = l.remove(0);
                            let end = l.remove(0);
                            Some(FrontValue::desugar_range(
                                range.range,
                                begin,
                                Some(second),
                                end,
                            )?)
                        }
                        v => {
                            l.insert(2, v);
                            None
                        }
                    }
                }
                _ => None,
            } {
                Some(value) => value,
                None => Value::List(
                    l.into_iter()
                        .map(FrontValue::desugar)
                        .collect::<Result<Vec<Value>>>()?,
                ),
            },
        )
    }

    fn desugar_apply(range: loc::Range<usize>, mut app: FrontApply) -> Result<Value> {
        Ok(
            match if app.args.len() == 2 {
                let mut argument_series = vec![];
                let mut v = app.args.remove(0);
                loop {
                    match v {
                        FrontValue::Apply(mut app2)
                            if app2.v.f == app.f && app2.v.args.len() == 2 =>
                        {
                            argument_series.push(app2.v.args.remove(1));
                            v = app2.v.args.remove(0);
                        }
                        _ => {
                            argument_series.push(v);
                            break;
                        }
                    }
                }
                match argument_series.iter().position(|value| match value {
                    FrontValue::Open(Front::RangeMark(_)) => true,
                    _ => false,
                }) {
                    None => {
                        app.args.insert(
                            0,
                            argument_series
                                .into_iter()
                                .rev()
                                .reduce(|left, right| {
                                    FrontValue::Apply(loc::Located {
                                        range,
                                        v: FrontApply {
                                            f: app.f.clone(),
                                            args: vec![left, right],
                                        },
                                    })
                                })
                                .unwrap(),
                        );
                        None
                    }
                    Some(index) => {
                        let range = match argument_series.remove(index) {
                            FrontValue::Open(Front::RangeMark(range)) => range.range,
                            _ => panic!("there should be a mark!"),
                        };
                        if index == argument_series.len() {
                            Err(error::Error::Located(loc::Located {
                                range,
                                v: error::Located::UnexpectedRange,
                            }))?
                        }
                        let mut argument_series: Vec<Value> = argument_series
                            .into_iter()
                            .rev()
                            .map(FrontValue::desugar)
                            .collect::<Result<Vec<Value>>>()?;
                        argument_series.push(app.args.remove(0).desugar()?);
                        let index = argument_series.len() - index - 1;
                        let begin = argument_series.remove(index - 1);
                        let end = argument_series.remove(index - 1);
                        let (context, zero_or_one) = Context::new_zero_or_one(&begin, &end);
                        if zero_or_one == ZeroOrOne::Zero {
                            Err(error::loc(range)(error::Located::EmptyRangeUnallowed))?
                        }
                        let begin = Context::extract(&context, begin).unwrap();
                        let end = Context::extract(&context, end).unwrap();
                        let (context, begin, second, index) = if index > 1 {
                            let previous = argument_series.remove(index - 2);
                            let (context0, _zero_or_one) =
                                Context::new_zero_or_one(&context, &previous);
                            /* context of context should have an hole */
                            let residual = Context::extract(&context0, context).unwrap();
                            match residual {
                                Context::Open(()) => {
                                    let previous = Context::extract(&context0, previous).unwrap();
                                    (context0, previous, Some(begin), index - 1)
                                }
                                residual => {
                                    argument_series.insert(index - 2, previous);
                                    (
                                        context0.fill(&mut Some(residual).into_iter()),
                                        begin,
                                        None,
                                        index,
                                    )
                                }
                            }
                        } else {
                            (context, begin, None, index)
                        };
                        let remake_apply = |left, right| {
                            Value::Apply(loc::Located {
                                range,
                                v: Apply {
                                    f: app.f.clone(),
                                    args: vec![left, right],
                                },
                            })
                        };
                        let initial_value = argument_series
                            .split_off(index - 1)
                            .into_iter()
                            .reduce(remake_apply);
                        let mut remaining_values = argument_series.into_iter();
                        let final_value = remaining_values.next();
                        let (initial_value, final_value) = match (initial_value, final_value) {
                            (None, None) => {
                                if app.f == "or" {
                                    (Some(Value::Constant(ConstantRef::from(false))), None)
                                } else if app.f == "and" {
                                    (Some(Value::Constant(ConstantRef::from(true))), None)
                                } else if app.f == "+" {
                                    (Some(Value::Constant(ConstantRef::from(0))), None)
                                } else if app.f == "*" {
                                    (Some(Value::Constant(ConstantRef::from(1))), None)
                                } else {
                                    (None, None)
                                }
                            }
                            pair => pair,
                        };
                        let value_range = Value::Open(Desugared::RangeApply {
                            f: app.f.clone(),
                            initial_value: initial_value.map(Box::new),
                            range: loc::Located {
                                range,
                                v: Box::new(Range {
                                    context: std::sync::Arc::new(context),
                                    begin,
                                    second,
                                    end,
                                }),
                            },
                            final_value: final_value.map(Box::new),
                        });
                        Some(remaining_values.fold(value_range, remake_apply))
                    }
                }
            } else {
                None
            } {
                Some(value) => value,
                None => Value::Apply(loc::Located {
                    range,
                    v: app.desugar()?,
                }),
            },
        )
    }

    pub fn desugar(self) -> Result<Value> {
        Ok(match self {
            FrontValue::Var(v) => Value::Var(v),
            FrontValue::Constant(c) => Value::Constant(c),
            FrontValue::List(l) => FrontValue::desugar_list(l)?,
            FrontValue::Dict(d) => Value::Dict(
                d.into_iter()
                    .map(|(k, v)| v.desugar().map(|v| (k.clone(), v)))
                    .collect::<Result<indexmap::IndexMap<String, Value>>>()?,
            ),
            FrontValue::Apply(app) => FrontValue::desugar_apply(app.range, app.v)?,
            FrontValue::MatchRegex(m) => {
                let v = m.v.0.desugar()?;
                Value::MatchRegex(loc::Located {
                    range: m.range,
                    v: (Box::new(v), m.v.1),
                })
            }
            FrontValue::If(if_) => Value::If(Box::new(loc::Located {
                range: if_.range,
                v: IfValue {
                    cond: if_.v.cond.desugar()?,
                    then_branch: if_.v.then_branch.desugar()?,
                    else_branch: if_.v.else_branch.desugar()?,
                },
            })),
            FrontValue::Open(front) => {
                let range = match front {
                    Front::Range(l) => l.range,
                    Front::RangeMark(l) => l.range,
                };
                return Err(error::Error::Located(loc::Located {
                    range: range.map(loc::Offset::Offset),
                    v: error::Located::UnexpectedRange,
                }));
            }
        })
    }
}

impl RuleTree {
    pub fn from_lines<I>(
        levels: &mut std::collections::HashSet<String>,
        lines: &mut std::iter::Peekable<I>,
    ) -> Result<loc::Located<loc::Offset, RuleTree>>
    where
        I: Iterator<Item = loc::Located<usize, Line>>,
    {
        let mut line: loc::Located<usize, Line> =
            lines.next().ok_or(error::Error::Located(loc::Located {
                range: loc::Range {
                    begin: loc::Offset::EndOfFile,
                    end: None,
                },
                v: error::Located::EmptyGroup,
            }))?;
        if levels.contains(&line.v.level) {
            return Err(error::Error::Located(loc::Located {
                range: line.range.map(loc::Offset::Offset),
                v: error::Located::EmptyGroup,
            }));
        }
        levels.insert(line.v.level.clone());
        let begin = loc::Offset::Offset(line.range.begin);
        let mut vec = Vec::new();
        loop {
            let rule: Rule<FrontValue, ()> = line.v.rule;
            match rule {
                Rule::Simple(simple_rule) => {
                    vec.push(loc::Located {
                        range: line.range.map(loc::Offset::Offset),
                        v: Rule::Simple(simple_rule.desugar()?),
                    });
                }
                Rule::Linked(()) => {
                    let sub_rules = RuleTree::from_lines(levels, lines)?;
                    vec.push(loc::Located {
                        range: loc::Range {
                            begin: loc::Offset::Offset(line.range.begin),
                            end: sub_rules.range.end,
                        },
                        v: Rule::Linked(sub_rules.v),
                    });
                }
                Rule::If(condition) => {
                    let sub_rules = RuleTree::from_lines(levels, lines)?;
                    vec.push(loc::Located {
                        range: loc::Range {
                            begin: loc::Offset::Offset(line.range.begin),
                            end: sub_rules.range.end,
                        },
                        v: Rule::If(If {
                            condition: condition.condition.desugar()?,
                            then: sub_rules.v,
                        }),
                    });
                }
                Rule::Collect(collect) => {
                    let sub_rules = RuleTree::from_lines(levels, lines)?;
                    vec.push(loc::Located {
                        range: line.range.map(loc::Offset::Offset),
                        v: Rule::Collect(Collect {
                            x: collect.x,
                            rules: sub_rules.v,
                        }),
                    });
                }
            }
            match lines.peek() {
                None => break,
                Some(peeked_line) => {
                    if peeked_line.v.level != line.v.level {
                        break;
                    }
                    /* we know that `line.next()` will succeed because line is
                    already peeked */
                    line = lines.next().unwrap();
                }
            }
        }
        levels.remove(&line.v.level);
        Ok(loc::Located {
            range: loc::Range {
                begin,
                end: line.range.end.map(loc::Offset::Offset),
            },
            v: RuleTree(vec),
        })
    }
}
