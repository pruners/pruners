/**
This file is part of Pruners.

Pruners is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or any later version.

Pruners is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Pruners. If not, see <https://www.gnu.org/licenses/>.

Copyright (C) 2023, Inria

Contacts:
Thierry Martinez <thierry.martinez@inria.fr>
Simon Legrand <simon.legrand@inria.fr>
*/
use crate::{ast, error, interpret};
use sha2::{self, Digest};

pub struct Context<'a, S: interpret::System> {
    pub system: &'a S,
    pub dict: &'a indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    pub env: &'a interpret::Env,
}

pub type F<S> = Box<
    dyn Fn(Context<S>, &Vec<ast::ConstantRef>) -> Result<ast::ConstantRef, error::Located>
        + std::marker::Send
        + std::marker::Sync,
>;

#[derive(Clone, Debug)]
pub struct Sign(pub Vec<ast::Type>);

impl Sign {
    pub fn is_subsign(&self, s: &Sign) -> bool {
        self.0.len() == s.0.len() && self.0.iter().zip(s.0.iter()).all(|(a, b)| a.is_subtype(b))
    }

    pub fn iter(&self) -> std::slice::Iter<'_, ast::Type> {
        self.0.iter()
    }
}

impl std::fmt::Display for Sign {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(fmt, "(")?;
        error::fmt_comma(fmt, &self.0)?;
        write!(fmt, ")")
    }
}

impl From<&Vec<ast::ConstantRef>> for Sign {
    fn from(args: &Vec<ast::ConstantRef>) -> Self {
        Sign(args.iter().map(|c| ast::Type::from(&**c)).collect())
    }
}

pub type Signed<S> = (Sign, F<S>);

pub struct Overloaded<S: interpret::System>(Vec<Signed<S>>);

pub fn check_singleton<Item, Iterator>(mut iter: Iterator) -> Option<Item>
where
    Iterator: std::iter::Iterator<Item = Item>,
{
    iter.next().filter(|_| iter.next().is_none())
}

impl<S: interpret::System> Overloaded<S> {
    pub fn find(&self, s: &Sign) -> Option<&F<S>> {
        let compatible: Vec<&(Sign, F<S>)> = self
            .0
            .iter()
            .filter(|(sign, _f)| s.is_subsign(sign))
            .collect();
        let most_precise = compatible
            .iter()
            .filter(|(sign, _f)| compatible.iter().all(|(sign0, _f0)| sign.is_subsign(sign0)));
        match check_singleton(most_precise) {
            None => None,
            Some((_sign, f)) => Some(f),
        }
    }

    pub fn iter(&self) -> std::slice::Iter<'_, Signed<S>> {
        self.0.iter()
    }
}

pub type Map<S> = std::collections::HashMap<String, Overloaded<S>>;

/* The following serie of functions make_* are simpler bindings for functions
 * that don't require Context .*/
fn make_unary_any<R, S: interpret::System>(
    f: Box<
        dyn Fn(&ast::ConstantRef) -> Result<R, error::Located>
            + std::marker::Send
            + std::marker::Sync,
    >,
) -> Signed<S>
where
    R: Into<ast::ConstantRef> + 'static,
{
    (
        Sign(vec![ast::Type::Any]),
        Box::new(move |_context, vec| f(&vec[0]).map(R::into)),
    )
}

fn make_unary<A, R, S: interpret::System>(
    f: Box<dyn Fn(A) -> Result<R, error::Located> + std::marker::Send + std::marker::Sync>,
) -> Signed<S>
where
    A: ast::TypedConversion + 'static,
    R: Into<ast::ConstantRef> + 'static,
{
    (
        Sign(vec![A::get_type()]),
        Box::new(move |_context, vec| f(A::try_from(&vec[0])?).map(R::into)),
    )
}

fn make_unary_ref<A, R, S: interpret::System>(
    f: Box<dyn Fn(&A) -> Result<R, error::Located> + std::marker::Send + std::marker::Sync>,
) -> Signed<S>
where
    A: ast::TypedConversionRef + 'static,
    R: Into<ast::ConstantRef> + 'static,
{
    (
        Sign(vec![A::get_type()]),
        Box::new(move |_context, vec| f(A::try_from(&vec[0])?).map(R::into)),
    )
}

fn make_bin<A0, A1, R, S: interpret::System>(
    f: Box<dyn Fn(A0, A1) -> Result<R, error::Located> + std::marker::Send + std::marker::Sync>,
) -> Signed<S>
where
    A0: ast::TypedConversion + 'static,
    A1: ast::TypedConversion + 'static,
    R: Into<ast::ConstantRef> + 'static,
{
    (
        Sign(vec![A0::get_type(), A1::get_type()]),
        Box::new(move |_context, vec| {
            f(A0::try_from(&vec[0])?, A1::try_from(&vec[1])?).map(R::into)
        }),
    )
}

fn make_bin_ref<'a, A0, A1, R, S: interpret::System>(
    f: Box<dyn Fn(&A0, &A1) -> Result<R, error::Located> + std::marker::Send + std::marker::Sync>,
) -> Signed<S>
where
    A0: ast::TypedConversionRef + 'static,
    A1: ast::TypedConversionRef + 'static,
    R: Into<ast::ConstantRef> + 'static,
{
    (
        Sign(vec![A0::get_type(), A1::get_type()]),
        Box::new(move |_context, vec| {
            f(A0::try_from(&vec[0])?, A1::try_from(&vec[1])?).map(R::into)
        }),
    )
}

fn make_ter<A0, A1, A2, R, S: interpret::System>(
    f: Box<dyn Fn(A0, A1, A2) -> Result<R, error::Located> + std::marker::Send + std::marker::Sync>,
) -> Signed<S>
where
    A0: ast::TypedConversion + 'static,
    A1: ast::TypedConversion + 'static,
    A2: ast::TypedConversion + 'static,
    R: Into<ast::ConstantRef> + 'static,
{
    (
        Sign(vec![A0::get_type(), A1::get_type(), A2::get_type()]),
        Box::new(move |_context, vec| {
            f(
                A0::try_from(&vec[0])?,
                A1::try_from(&vec[1])?,
                A2::try_from(&vec[2])?,
            )
            .map(R::into)
        }),
    )
}

fn compare_list(
    a: &Vec<ast::ConstantRef>,
    b: &Vec<ast::ConstantRef>,
) -> Result<std::cmp::Ordering, error::Located> {
    if a.len() != b.len() {
        return Err(error::Located::ListLengthMismatch);
    }
    for (a, b) in a.iter().zip(b.iter()) {
        match compare(a, b)? {
            std::cmp::Ordering::Equal => (),
            result => return Ok(result),
        }
    }
    Ok(std::cmp::Ordering::Equal)
}

fn compare(
    a: &ast::ConstantRef,
    b: &ast::ConstantRef,
) -> Result<std::cmp::Ordering, error::Located> {
    match (&**a, &**b) {
        (ast::Constant::Int(a), ast::Constant::Int(b)) => Ok((*a).cmp(b)),
        (ast::Constant::Int(a), ast::Constant::Float(b)) => (*a as f64)
            .partial_cmp(b)
            .ok_or(error::Located::CannotCompare),
        (ast::Constant::Float(a), ast::Constant::Int(b)) => (*a)
            .partial_cmp(&(*b as f64))
            .ok_or(error::Located::CannotCompare),
        (ast::Constant::Float(a), ast::Constant::Float(b)) => {
            (*a).partial_cmp(b).ok_or(error::Located::CannotCompare)
        }
        (ast::Constant::String(a), ast::Constant::String(b)) => Ok(a.cmp(b)),
        (ast::Constant::List(a), ast::Constant::List(b)) => compare_list(a, b),
        _ => Err(error::Located::Incomparable(a.clone(), b.clone())),
    }
}

fn make_compare<S: interpret::System>(
    f: Box<dyn Fn(std::cmp::Ordering) -> bool + std::marker::Send + std::marker::Sync>,
) -> Signed<S> {
    (
        Sign(vec![ast::Type::Any, ast::Type::Any]),
        Box::new(move |_context, vec| {
            compare(&vec[0], &vec[1]).map(|o| ast::ConstantRef::from(f(o)))
        }),
    )
}

fn make_eq<S: interpret::System>(
    f: Box<dyn Fn(bool) -> bool + std::marker::Send + std::marker::Sync>,
) -> Signed<S> {
    (
        Sign(vec![ast::Type::Any, ast::Type::Any]),
        Box::new(move |_context, vec| Ok(ast::ConstantRef::from(f(&vec[0] == &vec[1])))),
    )
}

/// Substitute variables in a template string.
///
/// Given an input string `template`, replace tokens of the form `${foo}` with
/// values provided in `variables`.
/*
fn substitute(
    template: &String,
    variables: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
) -> Result<String, error::Located> {
    let mut output = template.clone();
    for (k, v) in variables {
        validate(k.to_string(), "key".to_string())?;
        validate(v.to_string(), "value".to_string())?;

        let from = format!("${{{}}}", k);
        match &**v {
            ast::Constant::String(s) => {
                // Call to_string on &s: &str, instead of ConstantRef to avoid extra quotes
                output = output.replace(&from, &s.to_string());
            }
            _ => output = output.replace(&from, &v.to_string()),
        }
    }
    Ok(output)
}
*/
fn substitute(
    template: &str,
    variables: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
) -> Result<String, error::Located> {
    substitute_iter(&mut template.chars().peekable(), variables, false)
}

fn read_char_in_substitute<I>(
    template: &mut I,
    in_variable_name: bool,
) -> Result<Option<char>, error::Located>
where
    I: Iterator<Item = char>,
{
    match (template.next(), in_variable_name) {
        (None, true) => Err(error::Located::NonTerminatedVariable),
        (None, false) | (Some('}'), true) => Ok(None),
        (Some(c), _) => Ok(Some(c)),
    }
}

fn substitute_iter<I>(
    template: &mut I,
    variables: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    in_variable_name: bool,
) -> Result<String, error::Located>
where
    I: Iterator<Item = char>,
{
    let mut output = String::new();
    while let Some(c) = read_char_in_substitute(template, in_variable_name)? {
        match c {
            '\\' => match read_char_in_substitute(template, in_variable_name)? {
                None => {
                    output.push('\\');
                    break;
                }
                Some(c) => output.push(c),
            },
            '$' => {
                match read_char_in_substitute(template, in_variable_name)? {
                    None => {
                        output.push('$');
                        break;
                    }
                    Some('{') => {
                        let name = substitute_iter(template, &variables, true)?;
                        let value = variables
                            .get(&ast::Var::from(name.as_str()))
                            .ok_or_else(|| error::Located::UnboundVariable(name))?;
                        match &**value {
                            ast::Constant::String(s) =>
                            // Call to_string on &s: &str, instead of ConstantRef to avoid extra quotes
                            {
                                output.push_str(&s)
                            }
                            _ => output.push_str(&value.to_string()),
                        }
                    }
                    Some(c) => {
                        output.push('$');
                        output.push(c)
                    }
                }
            }
            _ => output.push(c),
        }
    }
    Ok(output)
}
/*
/// Validate variables for substitution.
///
/// This check whether substitution variables are valid. In order to make
/// substitution deterministic, the following characters are not allowed
/// within variables names nor values: `$`, `{`, `}`.
fn validate_vars(
    variables: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
) -> Result<(), error::Located> {
    for (k, v) in variables {
        validate(k.to_string(), "key".to_string())?;
        validate(v.to_string(), "value".to_string())?;
    }
    Ok(())
}

/// Check whether `value` contains invalid characters.
fn validate(value: String, kind: String) -> Result<(), error::Located> {
    let forbidden = &["$", "{", "}"];
    for forbidden_c in forbidden {
        if value.contains(forbidden_c) {
            return Err(error::Located::InvalidCharacter {
                kind: kind.to_string(),
                val: value.to_string(),
                c: forbidden_c.to_string(),
            });
        };
    }
    Ok(())
}
*/
pub fn map<S: interpret::System>() -> Map<S> {
    let mut result = std::collections::HashMap::new();
    result.insert(
        String::from("+"),
        Overloaded(vec![
            make_bin(Box::new(|a: i64, b: i64| Ok(a + b))),
            make_bin(Box::new(|a: f64, b: f64| Ok(a + b))),
            make_bin_ref(Box::new(|a: &String, b: &String| Ok(a.clone() + &*b))),
        ]),
    );
    result.insert(
        String::from("-"),
        Overloaded(vec![
            make_unary(Box::new(|a: i64| Ok(-a))),
            make_bin(Box::new(|a: i64, b: i64| Ok(a - b))),
            make_unary(Box::new(|a: f64| Ok(-a))),
            make_bin(Box::new(|a: f64, b: f64| Ok(a - b))),
        ]),
    );
    result.insert(
        String::from("*"),
        Overloaded(vec![
            make_bin(Box::new(|a: i64, b: i64| Ok(a * b))),
            make_bin(Box::new(|a: f64, b: f64| Ok(a * b))),
        ]),
    );
    result.insert(
        String::from("/"),
        Overloaded(vec![
            make_bin(Box::new(|a: i64, b: i64| {
                if b == 0 {
                    Err(error::Located::DivisionByZero)
                } else {
                    Ok(a / b)
                }
            })),
            make_bin(Box::new(|a: f64, b: f64| {
                if b == 0. {
                    Err(error::Located::DivisionByZero)
                } else {
                    Ok(a / b)
                }
            })),
        ]),
    );
    result.insert(
        String::from("not"),
        Overloaded(vec![make_unary(Box::new(|a: bool| Ok(!a)))]),
    );
    result.insert(
        String::from("or"),
        Overloaded(vec![make_bin(Box::new(|a: bool, b: bool| Ok(a || b)))]),
    );
    result.insert(
        String::from("and"),
        Overloaded(vec![make_bin(Box::new(|a: bool, b: bool| Ok(a && b)))]),
    );
    result.insert(
        String::from("="),
        Overloaded(vec![make_eq(Box::new(|o| o))]),
    );
    result.insert(
        String::from("!="),
        Overloaded(vec![make_eq(Box::new(|o| !o))]),
    );
    result.insert(
        String::from("<"),
        Overloaded(vec![make_compare(Box::new(|o| {
            o == std::cmp::Ordering::Less
        }))]),
    );
    result.insert(
        String::from("<="),
        Overloaded(vec![make_compare(Box::new(|o| {
            o != std::cmp::Ordering::Greater
        }))]),
    );
    result.insert(
        String::from(">"),
        Overloaded(vec![make_compare(Box::new(|o| {
            o == std::cmp::Ordering::Greater
        }))]),
    );
    result.insert(
        String::from(">="),
        Overloaded(vec![make_compare(Box::new(|o| {
            o != std::cmp::Ordering::Less
        }))]),
    );
    result.insert(
        String::from("type"),
        Overloaded(vec![make_unary_any(Box::new(|a| {
            Ok(ast::Type::from(&**a))
        }))]),
    );
    result.insert(
        String::from("str"),
        Overloaded(vec![make_unary_any(Box::new(|a| Ok(a.any_as_string())))]),
    );
    result.insert(
        String::from("range"),
        Overloaded(vec![
            make_bin(Box::new(|a: i64, b: i64| {
                Ok((a..b)
                    .map(ast::ConstantRef::from)
                    .collect::<Vec<ast::ConstantRef>>())
            })),
            make_ter(Box::new(|a: i64, b: i64, c: i64| match c > 0 {
                true => Ok((a..b)
                    .step_by(c as usize)
                    .map(ast::ConstantRef::from)
                    .collect::<Vec<ast::ConstantRef>>()),
                false => Err(error::Located::WrongSign),
            })),
        ]),
    );
    result.insert(
        String::from("read"),
        Overloaded(vec![(
            Sign(vec![ast::Type::String]),
            Box::new(move |context: Context<S>, vec| {
                let path = std::path::PathBuf::from(vec[0].as_string()?);
                Ok(ast::ConstantRef::from(
                    context.system.reader(&path).map_err(|error| {
                        error::Located::SubError(Box::new(error::Error::CouldNotReadFile {
                            filename: path.clone(),
                            error,
                        }))
                    })?,
                ))
            }),
        )]),
    );
    result.insert(
        String::from("get_field"),
        Overloaded(vec![
            make_bin_ref(Box::new(
                |dict: &indexmap::IndexMap<String, ast::ConstantRef>, field: &String| match dict
                    .get(field)
                {
                    None => Err(error::Located::UnknownField {
                        dict: dict.clone(),
                        field: field.clone(),
                    }),
                    Some(v) => Ok(v.clone()),
                },
            )),
            make_bin_ref(Box::new(|list: &Vec<ast::ConstantRef>, field: &String| {
                let dicts =
                    list.iter()
                        .map(|v| ast::TypedConversionRef::try_from(v))
                        .collect::<Result<
                            Vec<&indexmap::IndexMap<String, ast::ConstantRef>>,
                            error::Located,
                        >>()?;
                let result: Vec<ast::ConstantRef> = dicts
                    .iter()
                    .filter_map(|dict| dict.get(field).map(|v| v.clone()))
                    .collect();
                Ok(result)
            })),
        ]),
    );
    result.insert(
        String::from("get_item"),
        Overloaded(vec![make_bin_ref(Box::new(
            |list: &Vec<ast::ConstantRef>, index: &i64| {
                Ok(list
                    .get(*index as usize)
                    .ok_or_else(|| error::Located::IndexOutOfBounds {
                        list: list.clone(),
                        index: *index,
                    })?
                    .clone())
            },
        ))]),
    );
    result.insert(
        String::from("len"),
        Overloaded(vec![make_unary_ref(Box::new(
            |list: &Vec<ast::ConstantRef>| Ok(list.len() as i64),
        ))]),
    );
    result.insert(
        String::from("write"),
        Overloaded(vec![(
            Sign(vec![ast::Type::String, ast::Type::String]),
            Box::new(move |context, vec| {
                let path = std::path::PathBuf::from(vec[0].as_string()?);
                let value = vec[1].as_string()?;
                context.system.writer(&path, value)?;
                Ok(ast::ConstantRef::unit())
            }),
        )]),
    );
    result.insert(
        String::from("configure"),
        Overloaded(vec![(
            Sign(vec![ast::Type::String]),
            Box::new(move |context, vec| {
                let template = vec[0].as_string()?;
                let configured_template = substitute(template, context.dict)?;
                Ok(ast::ConstantRef::from(configured_template))
            }),
        )]),
    );
    result.insert(
        String::from("source_directory"),
        Overloaded(vec![(
            Sign(vec![]),
            Box::new(move |context, _vec| {
                Ok(ast::ConstantRef::from(
                    context.env.source_directory.to_str().unwrap(),
                ))
            }),
        )]),
    );
    result.insert(
        String::from("path"),
        Overloaded(vec![make_unary_ref(Box::new(
            |list: &Vec<ast::ConstantRef>| {
                let path = list
                    .iter()
                    .map(|v| v.as_string().map(std::path::Path::new))
                    .collect::<Result<std::path::PathBuf, error::Located>>()?;
                Ok(ast::ConstantRef::from(path.to_str().unwrap()))
            },
        ))]),
    );
    result.insert(
        String::from("rename"),
        Overloaded(vec![(
            Sign(vec![ast::Type::String, ast::Type::String]),
            Box::new(move |context, vec| {
                context.system.rename(
                    std::path::Path::new(vec[0].as_string()?),
                    std::path::Path::new(vec[1].as_string()?),
                )?;
                Ok(ast::ConstantRef::unit())
            }),
        )]),
    );
    result.insert(
        String::from("exists"),
        Overloaded(vec![(
            Sign(vec![ast::Type::String]),
            Box::new(move |context, vec| {
                Ok(ast::ConstantRef::from(
                    context
                        .system
                        .exists(std::path::Path::new(vec[0].as_string()?)),
                ))
            }),
        )]),
    );
    result.insert(
        String::from("to_json"),
        Overloaded(vec![
            (make_unary_ref(Box::new(|c: &ast::ConstantRef| {
                Ok(serde_json::to_string(c)?)
            }))),
        ]),
    );
    result.insert(
        String::from("from_json"),
        Overloaded(vec![
            (make_unary_ref(Box::new(|s: &String| {
                Ok(serde_json::from_str::<ast::ConstantRef>(s)?)
            }))),
        ]),
    );
    result.insert(
        String::from("hash"),
        Overloaded(vec![
            (make_bin_ref(Box::new(|s: &String, t: &String| {
                fn digest<D: Digest>(s: &String) -> Result<String, error::Located>
                where
                    sha2::digest::generic_array::GenericArray<u8, D::OutputSize>:
                        std::fmt::UpperHex,
                {
                    let mut hasher = D::new();
                    hasher.update(s.as_bytes());
                    Ok(format!("{:X}", hasher.finalize()))
                }
                match t.as_str() {
                    "sha256" => digest::<sha2::Sha256>(s),
                    "sha512" => digest::<sha2::Sha512>(s),
                    _ => {
                        panic!("POUET")
                    }
                }
            }))),
        ]),
    );
    result
}
