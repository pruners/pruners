/**
This file is part of Pruners.

Pruners is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or any later version.

Pruners is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Pruners. If not, see <https://www.gnu.org/licenses/>.

Copyright (C) 2023, Inria

Contacts:
Thierry Martinez <thierry.martinez@inria.fr>
Simon Legrand <simon.legrand@inria.fr>
*/
use lalrpop_util;

use crate::{ast, functions, loc};
use regex;

/// Errors associated to a position in the prune script. The location itself is
/// not embedded. Instantiated in the variant error::Error::Located.
#[derive(Debug)]
pub enum Located {
    EmptyGroup,
    UnboundVariable(String),
    UnknownFunction(String),
    ListLengthMismatch,
    CannotCompare,
    OnlyInrulesInLinkedSection,
    UnexpectedLevel,
    TypeMismatch {
        expected: ast::Type,
        constant: ast::ConstantRef,
    },
    FunctionUnavailable {
        f: String,
        args: Vec<ast::ConstantRef>,
        sign: Vec<functions::Sign>,
    },
    Incomparable(ast::ConstantRef, ast::ConstantRef),
    DivisionByZero,
    WrongSign,
    Regex(regex::Error),
    InvalidIntegerValue {
        text: String,
        error: std::num::ParseIntError,
    },
    InvalidFloatValue {
        text: String,
        error: std::num::ParseFloatError,
    },
    VariableExpected,
    UnknownField {
        dict: indexmap::IndexMap<String, ast::ConstantRef>,
        field: String,
    },
    IndexOutOfBounds {
        list: Vec<ast::ConstantRef>,
        index: i64,
    },
    IgnoredNonUnitResult {
        result: ast::ConstantRef,
    },
    IO(std::io::Error),
    UnexpectedRange,
    NullRangeStep,
    EmptyRangeUnallowed,
    Action(Action),
    SubError(Box<Error<u64>>),
    SerdeJson(serde_json::Error),
    NonTerminatedVariable,
}

pub fn fmt_comma<X>(fmt: &mut std::fmt::Formatter, v: &Vec<X>) -> std::fmt::Result
where
    X: std::fmt::Display,
{
    let mut iter = v.iter();
    match iter.next() {
        None => (),
        Some(first) => {
            write!(fmt, "{}", first)?;
            for item in iter {
                write!(fmt, ", {}", item)?;
            }
        }
    }
    Ok(())
}

impl std::fmt::Display for Located {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Located::EmptyGroup => write!(fmt, "Empty group"),
            Located::UnboundVariable(x) => write!(fmt, "Unbound variable {}", x),
            Located::UnknownFunction(x) => write!(fmt, "Unknown function {}", x),
            Located::ListLengthMismatch => write!(fmt, "List length mismatch"),
            Located::CannotCompare => write!(fmt, "Cannot compare"),
            Located::OnlyInrulesInLinkedSection => {
                write!(fmt, "Only in-rules are allowed in linked: section")
            }
            Located::UnexpectedLevel => write!(fmt, "Unexpected level"),
            Located::TypeMismatch { expected, constant } => write!(
                fmt,
                "Type {} expected but value {} of type {} given",
                expected,
                constant,
                ast::Type::from(&**constant)
            ),
            Located::FunctionUnavailable { f, args, sign } => {
                write!(fmt, "Function {} cannot be applied to (", f)?;
                fmt_comma(fmt, args)?;
                write!(fmt, ") of type (")?;
                fmt_comma(fmt, &args.iter().map(|c| ast::Type::from(&**c)).collect())?;
                write!(fmt, "), the following versions are available: ")?;
                fmt_comma(fmt, sign)
            }
            Located::Incomparable(a, b) => write!(
                fmt,
                "{} of type {} and {} of type {} are incomparable",
                a,
                ast::Type::from(&**a),
                b,
                ast::Type::from(&**b)
            ),
            Located::DivisionByZero => write!(fmt, "Division by zero"),
            Located::WrongSign => write!(fmt, "Wrong sign"),
            Located::Regex(error) => write!(fmt, "Invalid regular expression: {}", error),
            Located::InvalidIntegerValue { text, error } => {
                write!(fmt, "Invalid integer value {}: {}", text, error)
            }
            Located::InvalidFloatValue { text, error } => {
                write!(fmt, "Invalid float value {}: {}", text, error)
            }
            Located::VariableExpected => write!(fmt, "Variable expected"),
            Located::UnknownField { dict, field } => {
                write!(fmt, "There is no field \"{}\" in the dictionary: ", field)?;
                ast::format_dict(fmt, dict)
            }
            Located::IndexOutOfBounds { list, index } => {
                write!(fmt, "Index {index} is out of the bounds of the list: ")?;
                fmt_comma(fmt, list)
            }
            Located::IgnoredNonUnitResult { result } => {
                write!(fmt, "Result {} cannot be ignored", result)
            }
            Located::IO(error) => {
                write!(fmt, "{}", error)
            }
            Located::UnexpectedRange => {
                write!(fmt, "Unexpected range")
            }
            Located::NullRangeStep => {
                write!(
                    fmt,
                    "The first and the second values of a range should be distinct"
                )
            }
            Located::EmptyRangeUnallowed => {
                write!(fmt, "Empty range unallowed here")
            }
            Located::Action(action) => write!(fmt, "{}", action),
            Located::SubError(error) => write!(fmt, "{}", *error),
            Located::SerdeJson(error) => write!(fmt, "{}", *error),
            Located::NonTerminatedVariable => write!(fmt, "Non terminated variable name"),
        }
    }
}

/// All errors handled by the interpreter.
#[derive(Debug)]
pub enum Error<Location> {
    CouldNotReadFile {
        filename: std::path::PathBuf,
        error: std::io::Error,
    },
    ParseError(lalrpop_util::ParseError<Location, String, Box<Error<Location>>>),
    Located(loc::Located<Location, Located>),
    CommandLine(CommandLineError),
    ActionFailed(i64),
    Generic(Box<dyn std::error::Error + std::marker::Send + std::marker::Sync>),
}

#[derive(Debug)]
pub enum CommandLineError {
    BothSkipAndKeep,
    InsideCondition(Located),
}

impl<E: std::error::Error + std::marker::Send + std::marker::Sync + 'static, L> From<E>
    for Error<L>
{
    fn from(error: E) -> Self {
        Error::Generic(Box::new(error))
    }
}

impl std::fmt::Display for CommandLineError {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            CommandLineError::BothSkipAndKeep => {
                write!(fmt, "Skip and keep options are mutually exclusives")
            }
            CommandLineError::InsideCondition(err) => {
                write!(fmt, "Error inside condition: {err}")
            }
        }
    }
}

impl<Location> Error<Location> {
    pub fn from_parse_error(
        error: lalrpop_util::ParseError<Location, lalrpop_util::lexer::Token, Box<Error<Location>>>,
    ) -> Self {
        Error::ParseError(error.map_token(|token| format!("{}", token)))
    }

    pub fn map_location<L>(self, op: impl Fn(Location) -> L) -> Error<L> {
        match self {
            Error::CouldNotReadFile { filename, error } => {
                Error::CouldNotReadFile { filename, error }
            }
            Error::ParseError(error) => Error::ParseError(
                error
                    .map_location(&op)
                    .map_error(move |err| Box::new(err.map_location(op))),
            ),
            Error::Located(located) => Error::Located(located.map_location(op)),
            Error::ActionFailed(count) => Error::ActionFailed(count),
            Error::CommandLine(error) => Error::CommandLine(error),
            Error::Generic(error) => Error::Generic(error),
        }
    }
}

impl From<Error<usize>> for Error<loc::Offset> {
    fn from(error: Error<usize>) -> Self {
        error.map_location(loc::Offset::Offset)
    }
}

/// Handy utility to map Result<_,error::Located> to Result<_,error::Error>
/// in map_err.
pub fn loc<Location: Clone, L: Into<Located>>(
    range: loc::Range<Location>,
) -> impl Fn(L) -> Error<Location> {
    move |v| {
        Error::Located(loc::Located {
            range: range.clone(),
            v: v.into(),
        })
    }
}

impl<Location> std::fmt::Display for Error<Location>
where
    Location: std::fmt::Display,
{
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::CouldNotReadFile { filename, error } => {
                write!(fmt, "Could not read file {}: {}", filename.display(), error)
            }
            Error::ParseError(error) => write!(fmt, "Parse error: {}", error),
            Error::Located(error) => write!(fmt, "{}: {}", error.range, error.v),
            Error::ActionFailed(1) => write!(fmt, "One action failed"),
            Error::ActionFailed(count) => write!(fmt, "{} actions failed", count),
            Error::CommandLine(error) => write!(fmt, "Command line error: {}", error),
            Error::Generic(error) => write!(fmt, "{}", error),
        }
    }
}

#[derive(Debug)]
pub enum Action {
    Io(std::io::Error),
    ExitStatus(std::process::ExitStatus),
}

impl From<std::io::Error> for Action {
    fn from(error: std::io::Error) -> Self {
        Action::Io(error)
    }
}

impl std::fmt::Display for Action {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Action::Io(error) => write!(f, "unable to execute the process: {}", error),
            Action::ExitStatus(error) => write!(f, "process failed: {}", error),
        }
    }
}

impl From<std::io::Error> for Located {
    fn from(error: std::io::Error) -> Self {
        Located::IO(error)
    }
}

impl From<Action> for Located {
    fn from(action: Action) -> Self {
        Located::Action(action)
    }
}

impl From<serde_json::Error> for Located {
    fn from(error: serde_json::Error) -> Self {
        Located::SerdeJson(error)
    }
}
