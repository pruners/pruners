/**
This file is part of Pruners.

Pruners is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or any later version.

Pruners is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Pruners. If not, see <https://www.gnu.org/licenses/>.

Copyright (C) 2023, Inria

Contacts:
Thierry Martinez <thierry.martinez@inria.fr>
Simon Legrand <simon.legrand@inria.fr>
*/
use crate::args::prune;
use crate::{ast, error, functions, loc};

#[derive(Debug, Clone, Copy)]
pub enum ConditionKind {
    Keep,
    Skip,
}

impl std::fmt::Display for ConditionKind {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            ConditionKind::Keep => write!(fmt, "keep"),
            ConditionKind::Skip => write!(fmt, "skip"),
        }
    }
}

pub struct Condition {
    pub op: ConditionKind,
    pub value: ast::Value,
}

pub trait System: std::marker::Send + std::marker::Sync {
    fn exists(&self, path: &std::path::Path) -> bool;
    fn rename(&self, src: &std::path::Path, tgt: &std::path::Path) -> std::io::Result<()>;
    fn reader(&self, path: &std::path::Path) -> std::io::Result<String>;
    fn writer(&self, path: &std::path::Path, contents: &str) -> std::io::Result<()>;
}

pub struct Context<S: System> {
    pub system: S,
    pub functions: functions::Map<S>,
    pub condition: Option<Condition>,
    pub execute_semaphore: async_semaphore::Semaphore,
}

pub struct Env {
    pub source_directory: std::path::PathBuf,
}

fn interpret_internal_apply<S: System>(
    context: &Context<S>,
    env: &Env,
    dict: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    range: &loc::Range<usize>,
    name: &str,
    args: &Vec<ast::ConstantRef>,
) -> ast::Result<ast::ConstantRef> {
    let f = context
        .functions
        .get(name)
        .ok_or(error::Error::Located(loc::Located {
            range: range.map(loc::Offset::Offset),
            v: error::Located::UnknownFunction(name.to_string()),
        }))?;
    let s = functions::Sign::from(args);
    match f.find(&s) {
        Some(instance) => instance(
            functions::Context {
                system: &context.system,
                env,
                dict,
            },
            args,
        )
        .map_err(error::loc(range.map(loc::Offset::Offset))),
        None => {
            // When a function `f` is applied over arguments `x_1`, ..., `x_n` of
            // types `t_1`, ..., `t_n`,
            // if there is no overload of `f` available with signature (`t_1`, ..., `t_n`),
            // then, instead of failing directly,
            // we check if there exists a _unique_ argument `x_i` such that:
            // - `x_i` is a list, the elements of which has type `t'_i`
            //   (we compute the least upperbound of the types of the elements:
            //   i.e., if the list contains `int`s and `float`s, the type is `float`);
            // - there is an overload of `f` available for the signature (`t_1`, ...,
            //   `t'_i`, ... `t_n`) (i.e., we substitute the type of list `t_i`
            //   by the type of its elements).
            // If such an argument `x_i` exists and is unique (we reject
            // ambiguities), then,
            // by denoting `x_{i,1}, ..., x_{i,k}` the elements of the list `x_i`,
            // we compute the list
            // `[f(x_1, ...,x_{i,1}, ..., x_n), ..., f(x_1, ...,x_{i,k}, ..., x_n)]`.
            let mappable = s.iter().enumerate().filter_map(|(i, ty)| match ty {
                ast::Type::List => {
                    let mut s_map = s.clone();
                    let items = args[i].as_list().unwrap(); // cannot fail because type is List
                    let items_ty = items.iter().fold(ast::Type::Empty, |ty, arg| {
                        ast::Type::least_upperbound(&ty, &ast::Type::from(&**arg))
                    });
                    s_map.0[i] = items_ty;
                    f.find(&s_map).map(|instance| (i, items, instance))
                }
                _ => None,
            });
            match functions::check_singleton(mappable) {
                None => Err(error::Error::Located(loc::Located {
                    range: range.map(loc::Offset::Offset),
                    v: error::Located::FunctionUnavailable {
                        f: name.to_string(),
                        args: args.clone(),
                        sign: f.iter().map(|(sign, _f)| sign.clone()).collect(),
                    },
                })),
                Some((i, items, instance)) => {
                    let new_list = items
                        .iter()
                        .map(|arg| {
                            let mut subst_args = args.clone();
                            subst_args[i] = arg.clone();
                            instance(
                                functions::Context {
                                    system: &context.system,
                                    env,
                                    dict,
                                },
                                &subst_args,
                            )
                        })
                        .collect::<Result<Vec<ast::ConstantRef>, error::Located>>()
                        .map_err(error::loc(range.map(loc::Offset::Offset)))?;
                    Ok(ast::ConstantRef::from(new_list))
                }
            }
        }
    }
}

fn interpret_apply<S: System>(
    context: &Context<S>,
    env: &Env,
    dict: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    apply: &loc::Located<usize, ast::Apply>,
) -> ast::Result<ast::ConstantRef> {
    if apply.v.f == "defined" && apply.v.args.len() == 1 {
        match match &apply.v.args[0] {
            ast::Value::Var(x) => Some(dict.contains_key(&x.v)),
            ast::Value::Apply(apply) => {
                if apply.v.f == "get_field" && apply.v.args.len() == 2 {
                    let expr = interpret_value(context, env, dict, &apply.v.args[0])?;
                    let expr = expr
                        .as_dict()
                        .map_err(error::loc(apply.range.map(loc::Offset::Offset)))?;
                    let field = interpret_value(context, env, dict, &apply.v.args[1])?;
                    let field = field
                        .as_string()
                        .map_err(error::loc(apply.range.map(loc::Offset::Offset)))?;
                    Some(expr.contains_key(field))
                } else {
                    None
                }
            }
            _ => None,
        } {
            None => Err(error::loc(apply.range)(error::Located::VariableExpected))?,
            Some(b) => Ok(ast::ConstantRef::from(b)),
        }
    } else if apply.v.f == "or" && apply.v.args.len() == 2 {
        let a = interpret_value(context, env, dict, &apply.v.args[0])?;
        if a.as_bool()
            .map_err(error::loc(apply.range.map(loc::Offset::Offset)))?
        {
            Ok(a)
        } else {
            interpret_value(context, env, dict, &apply.v.args[1])
        }
    } else if apply.v.f == "and" && apply.v.args.len() == 2 {
        let a = interpret_value(context, env, dict, &apply.v.args[0])?;
        if a.as_bool()
            .map_err(error::loc(apply.range.map(loc::Offset::Offset)))?
        {
            interpret_value(context, env, dict, &apply.v.args[1])
        } else {
            Ok(a)
        }
    } else {
        let args = apply
            .v
            .args
            .iter()
            .map(|value| interpret_value(context, env, dict, value))
            .collect::<ast::Result<Vec<ast::ConstantRef>>>()?;
        interpret_internal_apply(context, env, dict, &apply.range, &apply.v.f, &args)
    }
}

fn interpret_range<S: System>(
    context: &Context<S>,
    env: &Env,
    dict: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    range: loc::Range<usize>,
    value_range: &ast::Range,
) -> ast::Result<Vec<ast::ConstantRef>> {
    let begin = interpret_value(context, env, dict, &value_range.begin)?;
    let end = interpret_value(context, env, dict, &value_range.end)?;
    let vec = match &value_range.second {
        None => {
            let begin = begin.as_int().map_err(error::loc(range))?;
            let end = end.as_int().map_err(error::loc(range))?;
            (begin..(end + 1))
                .map(ast::ConstantRef::from)
                .collect::<Vec<ast::ConstantRef>>()
        }
        Some(second) => {
            let second = interpret_value(context, env, dict, &second)?;
            match (&*begin, &*second, &*end) {
                (
                    ast::Constant::Int(begin),
                    ast::Constant::Int(second),
                    ast::Constant::Int(end),
                ) => {
                    let step = second - begin;
                    if step > 0 {
                        (*begin..(*end + 1))
                            .step_by(step as usize)
                            .map(ast::ConstantRef::from)
                            .collect::<Vec<ast::ConstantRef>>()
                    } else if step < 0 {
                        (*end..(*begin + 1))
                            .step_by((-step) as usize)
                            /* StepBy does not support rev */
                            .collect::<Vec<i64>>()
                            .into_iter()
                            .rev()
                            .map(ast::ConstantRef::from)
                            .collect::<Vec<ast::ConstantRef>>()
                    } else {
                        Err(error::loc(range)(error::Located::NullRangeStep))?
                    }
                }
                _ => {
                    let begin = begin.as_float().map_err(error::loc(range))?;
                    let second = second.as_float().map_err(error::loc(range))?;
                    let end = end.as_float().map_err(error::loc(range))?;
                    let step = second - begin;
                    let mut result = vec![];
                    let mut value = begin;
                    if step > 0.0 {
                        while value <= end {
                            result.push(ast::ConstantRef::from(value));
                            value += step;
                        }
                    } else if step < 0.0 {
                        while value >= end {
                            result.push(ast::ConstantRef::from(value));
                            value -= step;
                        }
                    } else {
                        Err(error::loc(range)(error::Located::NullRangeStep))?
                    }
                    result
                }
            }
        }
    };
    Ok(vec
        .into_iter()
        .map(|c| {
            interpret_value(
                context,
                env,
                dict,
                &value_range
                    .context
                    .fill(&mut Some(ast::Value::Constant(c)).into_iter()),
            )
        })
        .collect::<ast::Result<Vec<ast::ConstantRef>>>()?)
}

fn interpret_desugared<S: System>(
    context: &Context<S>,
    env: &Env,
    dict: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    desugared: &ast::Desugared,
) -> ast::Result<ast::ConstantRef> {
    Ok(match &desugared {
        ast::Desugared::Range(range) => {
            ast::ConstantRef::from(interpret_range(context, env, dict, range.range, &range.v)?)
        }
        ast::Desugared::RangeApply {
            f,
            initial_value,
            range,
            final_value,
        } => {
            let vec = interpret_range(context, env, dict, range.range, &range.v)?;
            let initial_value = match initial_value {
                None => None,
                Some(v) => Some(interpret_value(context, env, dict, &v)?),
            };
            let final_value = match final_value {
                None => None,
                Some(v) => Some(interpret_value(context, env, dict, &v)?),
            };
            let mut iter = initial_value
                .iter()
                .chain(vec.iter())
                .chain(final_value.iter());
            let init = iter.next().ok_or_else(|| {
                error::loc(range.range.map(loc::Offset::Offset))(
                    error::Located::EmptyRangeUnallowed,
                )
            })?;
            iter.try_fold(init.clone(), |a, b| {
                interpret_internal_apply(
                    context,
                    env,
                    dict,
                    &range.range,
                    f,
                    &vec![a.clone(), b.clone()],
                )
            })?
        }
    })
}

fn interpret_value<S: System>(
    context: &Context<S>,
    env: &Env,
    dict: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    value: &ast::Value,
) -> ast::Result<ast::ConstantRef> {
    match value {
        ast::Value::Var(x) => match dict.get(&(*x).v) {
            None => Err(error::Error::Located(loc::Located {
                range: (*x).range.map(loc::Offset::Offset),
                v: error::Located::UnboundVariable((*x.v.0).clone()),
            })),
            Some(value) => Ok(value.clone()),
        },
        ast::Value::Constant(constant) => Ok(constant.clone()),
        ast::Value::List(list) => Ok(ast::ConstantRef::from(
            list.iter()
                .map(|value| interpret_value(context, env, dict, value))
                .collect::<ast::Result<Vec<ast::ConstantRef>>>()?,
        )),
        ast::Value::Dict(dict_) => Ok(ast::ConstantRef::from(
            dict_
                .iter()
                .map(|(key, value)| Ok((key.clone(), interpret_value(context, env, dict, value)?)))
                .collect::<ast::Result<indexmap::IndexMap<String, ast::ConstantRef>>>()?,
        )),
        ast::Value::Apply(apply) => interpret_apply(context, env, dict, &apply),
        ast::Value::MatchRegex(m) => {
            let constant = interpret_value(context, env, dict, &*m.v.0)?;
            let s = constant
                .as_string()
                .map_err(error::loc(m.range.map(loc::Offset::Offset)))?;
            Ok(ast::ConstantRef::from(m.v.1.is_match(&s)))
        }
        ast::Value::If(if_value) => {
            let cond = interpret_value(context, env, dict, &if_value.v.cond)?;
            if cond
                .as_bool()
                .map_err(error::loc(if_value.range.map(loc::Offset::Offset)))?
            {
                interpret_value(context, env, dict, &if_value.v.then_branch)
            } else {
                interpret_value(context, env, dict, &if_value.v.else_branch)
            }
        }
        ast::Value::Open(desugared) => interpret_desugared(context, env, dict, desugared),
    }
}

#[async_trait::async_trait]
pub trait Act<S: System>: std::marker::Send + std::marker::Sync {
    async fn act(
        &self,
        context: &Context<S>,
        dict: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    ) -> ast::Result<()>;
}

pub struct ActList {
    list: std::sync::Mutex<Vec<indexmap::IndexMap<ast::Var, ast::ConstantRef>>>,
}

impl ActList {
    pub fn new() -> Self {
        ActList {
            list: std::sync::Mutex::new(Vec::new()),
        }
    }

    pub fn unwrap(
        self,
    ) -> std::sync::LockResult<Vec<indexmap::IndexMap<ast::Var, ast::ConstantRef>>> {
        return self.list.into_inner();
    }
}

#[async_trait::async_trait]
impl<S: System> Act<S> for ActList {
    async fn act(
        &self,
        _context: &Context<S>,
        dict: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    ) -> ast::Result<()> {
        self.list.lock().unwrap().push(dict.clone());
        Ok(())
    }
}

#[async_recursion::async_recursion]
pub async fn interpret_rules<'a: 'async_recursion, S: System>(
    context: &Context<S>,
    act: &impl Act<S>,
    env: &Env,
    dict: indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    mut rules: std::collections::VecDeque<
        &'a loc::Located<loc::Offset, ast::Rule<ast::Value, ast::RuleTree>>,
    >,
) -> ast::Result<()> {
    //Evaluate skip and keep conditions
    if let Some(condition) = &context.condition {
        match interpret_value(context, env, &dict, &condition.value) {
            Ok(cond) => {
                match (
                    &condition.op,
                    cond.as_bool().map_err(|err| {
                        error::Error::CommandLine::<loc::Offset>(
                            error::CommandLineError::InsideCondition(err),
                        )
                    })?,
                ) {
                    (ConditionKind::Keep, false) | (ConditionKind::Skip, true) => return Ok(()),
                    (ConditionKind::Keep, true) | (ConditionKind::Skip, false) => (),
                }
            }
            Err(_) => (),
        }
    };
    match rules.pop_front() {
        None => act.act(&context, &dict).await,
        Some(rule) => match &rule.v {
            ast::Rule::Simple(simple_rule) => match simple_rule {
                ast::SimpleRule::Assign(assignment) => {
                    let value = interpret_value(context, env, &dict, &assignment.value)?;
                    let mut dict = dict.clone();
                    dict.insert(assignment.x.clone(), value);
                    interpret_rules(context, act, env, dict, rules).await
                }
                ast::SimpleRule::In(assignment) => {
                    let list = interpret_value(context, env, &dict, &assignment.value)?;
                    futures::future::join_all(
                        list.as_list()
                            .map_err(error::loc(rule.range))?
                            .iter()
                            .map(|value| {
                                let mut dict = dict.clone();
                                dict.insert(assignment.x.clone(), value.clone());
                                interpret_rules(context, act, env, dict, rules.clone())
                            }),
                    )
                    .await
                    .into_iter()
                    .collect::<Result<Vec<()>, _>>()?;
                    Ok(())
                }
                ast::SimpleRule::Include(value) => {
                    let constant = interpret_value(context, env, &dict, &value)?;
                    let path = std::path::PathBuf::from(
                        constant.as_string().map_err(error::loc(rule.range))?,
                    );

                    let content =
                        context
                            .system
                            .reader(&path)
                            .map_err(|error| error::Error::<loc::Offset>::CouldNotReadFile {
                                filename: path.clone(),
                                error,
                            })?;
                    let source_directory = match path.parent() {
                        None => env.source_directory.clone(),
                        Some(parent) => env.source_directory.join(parent),
                    };
                    execute_string_raw(
                        context,
                        act,
                        &Env { source_directory },
                        dict,
                        &content,
                        rules,
                    )
                    .await
                }
                ast::SimpleRule::Apply(apply) => {
                    if apply.v.f == "execute" && apply.v.args.len() == 1 {
                        let command = interpret_value(context, env, &dict, &apply.v.args[0])?;
                        crate::execute_process(
                            &context.execute_semaphore,
                            command.as_string().map_err(error::loc(apply.range))?,
                            &dict,
                        )
                        .await
                        .map_err(error::loc(apply.range))?;
                        interpret_rules(context, act, env, dict, rules).await
                    } else {
                        let result = interpret_apply(context, env, &dict, &apply)?;
                        if *result == ast::Constant::Unit {
                            interpret_rules(context, act, env, dict, rules).await
                        } else {
                            Err(error::Error::Located(loc::Located {
                                range: rule.range,
                                v: error::Located::IgnoredNonUnitResult { result },
                            }))
                        }
                    }
                }
            },
            ast::Rule::Linked(sub_rules) => {
                let lists: Vec<(ast::Var, Vec<ast::ConstantRef>)> = sub_rules
                    .0
                    .iter()
                    .map(|rule| match &rule.v {
                        ast::Rule::Simple(ast::SimpleRule::In(assignment)) => {
                            let list = interpret_value(context, env, &dict, &assignment.value)?;
                            Ok((
                                assignment.x.clone(),
                                list.as_list().map_err(error::loc(rule.range))?.clone(),
                            ))
                        }
                        _ => Err(error::Error::Located(loc::Located {
                            range: rule.range,
                            v: error::Located::OnlyInrulesInLinkedSection,
                        })),
                    })
                    .collect::<ast::Result<Vec<(ast::Var, Vec<ast::ConstantRef>)>>>()?;
                let mut iters: Vec<(ast::Var, std::slice::Iter<ast::ConstantRef>)> = lists
                    .iter()
                    .map(|(x, list)| (x.clone(), list.iter()))
                    .collect::<Vec<(ast::Var, std::slice::Iter<ast::ConstantRef>)>>();
                loop {
                    let assignments_opt = iters
                        .iter_mut()
                        .map(|(x, iter)| match (*iter).next() {
                            None => Ok(None),
                            Some(value) => Ok(Some(ast::Assignment {
                                x: x.clone(),
                                value: value.clone(),
                            })),
                        })
                        .collect::<ast::Result<Vec<Option<ast::Assignment<ast::ConstantRef>>>>>()?;
                    if assignments_opt.iter().all(|x| x.is_none()) {
                        break;
                    }
                    match assignments_opt
                        .into_iter()
                        .collect::<Option<Vec<ast::Assignment<ast::ConstantRef>>>>()
                    {
                        Some(assignments) => {
                            let mut dict = dict.clone();
                            for assignment in assignments {
                                dict.insert(assignment.x, assignment.value);
                            }
                            interpret_rules(context, act, env, dict, rules.clone()).await?;
                        }
                        None => {
                            return Err(error::Error::Located(loc::Located {
                                range: rule.range,
                                v: error::Located::ListLengthMismatch,
                            }));
                        }
                    }
                }
                Ok(())
            }
            ast::Rule::If(condition) => {
                let constant = interpret_value(context, env, &dict, &condition.condition)?;
                if constant.as_bool().map_err(error::loc(rule.range))? {
                    let mut new_rules = std::collections::VecDeque::new();
                    new_rules.extend(condition.then.0.iter());
                    new_rules.extend(rules.iter());
                    interpret_rules(context, act, env, dict, new_rules).await
                } else {
                    interpret_rules(context, act, env, dict, rules).await
                }
            }
            ast::Rule::Collect(collect) => {
                let len_prefix = dict.len();
                let act_list = ActList::new();
                let mut sub_rules = std::collections::VecDeque::new();
                sub_rules.extend(collect.rules.0.iter());
                interpret_rules(context, &act_list, env, dict.clone(), sub_rules).await?;
                let mut dict = dict.clone();
                let list: Vec<indexmap::IndexMap<String, ast::ConstantRef>> = act_list
                    .unwrap()
                    .unwrap()
                    .into_iter()
                    .map(|dict| {
                        dict.iter()
                            .skip(len_prefix)
                            .map(|(x, v)| ((*x.0).clone(), v.clone()))
                            .collect()
                    })
                    .collect();
                dict.insert(collect.x.clone(), ast::ConstantRef::from(list));
                interpret_rules(context, act, env, dict, rules).await
            }
        },
    }
}

pub async fn execute_string_raw<'a, S: System>(
    context: &Context<S>,
    act: &impl Act<S>,
    env: &Env,
    dict: indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    content: &str,
    tail: std::collections::VecDeque<
        &'a loc::Located<loc::Offset, ast::Rule<ast::Value, ast::RuleTree>>,
    >,
) -> ast::Result<()> {
    let lines = prune::FileParser::new()
        .parse(&content)
        .map_err(|error| error::Error::from_parse_error(error).map_location(loc::Offset::Offset))?;
    let mut iter = lines.into_iter().peekable();
    let ast = ast::RuleTree::from_lines(&mut std::collections::HashSet::new(), &mut iter)?;
    let mut rules = std::collections::VecDeque::new();
    rules.extend(ast.v.0.iter());
    rules.extend(tail.into_iter());
    interpret_rules(context, act, env, dict, rules).await?;
    match iter.next() {
        None => Ok(()),
        Some(line) => Err(error::Error::Located(loc::Located {
            range: line.range.map(loc::Offset::Offset),
            v: error::Located::UnexpectedLevel,
        })),
    }
}

pub async fn execute_string<S: System>(
    context: Context<S>,
    act: impl Act<S>,
    env: Env,
    dict: indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    content: String,
) -> ast::Result<()> {
    execute_string_raw(
        &context,
        &act,
        &env,
        dict,
        &content,
        std::collections::VecDeque::new(),
    )
    .await
}
