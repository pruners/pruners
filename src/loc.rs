/**
This file is part of Pruners.

Pruners is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or any later version.

Pruners is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Pruners. If not, see <https://www.gnu.org/licenses/>.

Copyright (C) 2023, Inria

Contacts:
Thierry Martinez <thierry.martinez@inria.fr>
Simon Legrand <simon.legrand@inria.fr>
*/
use crate::interpret;

#[derive(Debug)]
pub struct LineColumn {
    origin: Origin,
    line: usize,
    column: usize,
}

#[derive(Debug)]
pub enum Origin {
    File(std::path::PathBuf),
    CommandLine(interpret::ConditionKind),
}

impl std::fmt::Display for Origin {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Origin::File(filename) => write!(fmt, "file {}", filename.display()),
            Origin::CommandLine(condition_kind) => write!(fmt, "option --{}", condition_kind),
        }
    }
}

impl LineColumn {
    pub fn from_offset(origin: Origin, offset: usize, s: &str) -> Self {
        let mut line = 1;
        let mut bol = 0;
        for (index, char) in s.chars().take(offset).enumerate() {
            if char == '\n' {
                line += 1;
                bol = index;
            }
        }
        LineColumn {
            origin,
            line,
            column: offset - bol,
        }
    }

    pub fn from_offset2(origin: Origin, offset: Offset, s: &str) -> Self {
        let u = match offset {
            Offset::EndOfFile => s.len(),
            Offset::Offset(o) => o,
        };
        Self::from_offset(origin, u, s)
    }
}

impl std::fmt::Display for LineColumn {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            fmt,
            "{}, line {}, character {}",
            self.origin, self.line, self.column
        )
    }
}

#[derive(Clone, Debug)]
pub struct Range<Location> {
    pub begin: Location,
    pub end: Option<Location>,
}

/*
impl<Location> Clone for Range<Location>
where Location: Clone {
    fn clone(&self) -> Self {
        Range { begin: self.begin.clone(), end: self.end.clone() }
    }
}
*/

impl<Location> Copy for Range<Location> where Location: Copy {}

impl<Location> Range<Location> {
    pub fn map<L>(self, mut op: impl FnMut(Location) -> L) -> Range<L> {
        Range {
            begin: op(self.begin),
            end: self.end.map(|l| op(l)),
        }
    }
}

impl<Location> std::fmt::Display for Range<Location>
where
    Location: std::fmt::Display,
{
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self.end {
            None => write!(fmt, "{}", self.begin),
            Some(ref end) => write!(fmt, "{}:{}", self.begin, end),
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Offset {
    EndOfFile,
    Offset(usize),
}

#[derive(Clone, Debug)]
pub struct Located<Location, V> {
    pub range: Range<Location>,
    pub v: V,
}

/*
impl<Location, V> Clone for Located<Location, V>
where Location: Clone, V: Clone {
    fn clone(&self) -> Self {
        Located { range: self.range.clone(), v: self.v.clone() }
    }
}
*/

impl<Location, V> Located<Location, V> {
    pub fn map<U>(self, op: impl FnOnce(V) -> U) -> Located<Location, U> {
        Located {
            range: self.range,
            v: op(self.v),
        }
    }

    pub fn map_location<L>(self, op: impl FnMut(Location) -> L) -> Located<L, V> {
        Located {
            range: self.range.map(op),
            v: self.v,
        }
    }
}

// impl<Location, V> Located<Location, V>
// where
//     Location: Clone,
// {
//     pub fn change<U>(&self, v: U) -> Located<Location, U> {
//         Located {
//             range: self.range.clone(),
//             v,
//         }
//     }
// }

impl From<usize> for Offset {
    fn from(u: usize) -> Self {
        Offset::Offset(u)
    }
}
