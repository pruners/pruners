/**
This file is part of Pruners.

Pruners is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or any later version.

Pruners is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY, without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Pruners. If not, see <https://www.gnu.org/licenses/>.

Copyright (C) 2023, Inria

Contacts:
Thierry Martinez <thierry.martinez@inria.fr>
Simon Legrand <simon.legrand@inria.fr>
*/
#[macro_use]
extern crate lalrpop_util;

mod args;
mod ast;
mod error;
mod functions;
mod interpret;
mod loc;

use clap::Parser;

lalrpop_mod!(pub prune);

fn action_print(dict: &indexmap::IndexMap<ast::Var, ast::ConstantRef>) -> () {
    print!("{{");
    let mut iter = dict.iter();
    match iter.next() {
        None => (),
        Some((x, value)) => {
            print!("{} = {}", x, value);
            for (x, value) in iter {
                print!(", {} = {}", x, value);
            }
        }
    }
    println!("}}");
    ()
}

fn shell_execute(command: &str) -> tokio::process::Command {
    if cfg!(target_os = "windows") {
        let mut result = tokio::process::Command::new("cmd");
        result.args(["/C", command]);
        result
    } else {
        let mut result = tokio::process::Command::new("bash");
        result.args(["-c", command]);
        result
    }
}

async fn execute_process(
    semaphore: &async_semaphore::Semaphore,
    command_str: &str,
    dict: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
) -> Result<(), error::Action> {
    let mut command = shell_execute(command_str);
    for (key, value) in dict.iter() {
        command.env((*key.0).clone(), value.any_as_string());
    }
    let _lock = semaphore.acquire().await;
    let status = command.status().await?;
    if status.success() {
        Ok(())
    } else {
        Err(error::Action::ExitStatus(status))
    }
}

pub struct ConcreteSystem {}

impl interpret::System for ConcreteSystem {
    fn exists(&self, path: &std::path::Path) -> bool {
        path.exists()
    }

    fn rename(&self, src: &std::path::Path, tgt: &std::path::Path) -> std::io::Result<()> {
        std::fs::rename(src, tgt)
    }

    fn reader(&self, path: &std::path::Path) -> std::io::Result<String> {
        std::fs::read_to_string(path)
    }

    fn writer(&self, path: &std::path::Path, contents: &str) -> std::io::Result<()> {
        if let Some(p) = path.parent() {
            std::fs::create_dir_all(p)?;
        }
        std::fs::write(path, contents)
    }
}

struct MainAct {
    print_combinations: bool,
    exec: Option<String>,
    failures: std::sync::Arc<std::sync::Mutex<i64>>,
}

#[async_trait::async_trait]
impl interpret::Act<ConcreteSystem> for MainAct {
    async fn act(
        &self,
        context: &interpret::Context<ConcreteSystem>,
        dict: &indexmap::IndexMap<ast::Var, ast::ConstantRef>,
    ) -> ast::Result<()> {
        if self.print_combinations {
            action_print(dict);
        }
        (match &self.exec {
            None => Ok(()),
            Some(command) => execute_process(&context.execute_semaphore, command, dict).await,
        })
        .or_else(|err| {
            println!("Error while executing action for {:?}: {}", dict, err);
            *self.failures.lock().unwrap() += 1;
            Ok(())
        })
    }
}

pub fn execute_content(
    act: impl interpret::Act<ConcreteSystem>,
    args: &args::Cli,
    content: String,
) -> Result<(), error::Error<loc::LineColumn>> {
    let filename = &args.script;
    let condition = match (&args.skip, &args.keep) {
        (None, None) => None,
        (Some(s), None) => Some((interpret::ConditionKind::Skip, s)),
        (None, Some(k)) => Some((interpret::ConditionKind::Keep, k)),
        (Some(_), Some(_)) => Err(error::Error::CommandLine(
            error::CommandLineError::BothSkipAndKeep,
        ))?,
    };
    let condition = condition
        .map(|(ck, cs)| {
            Ok::<interpret::Condition, error::Error<loc::LineColumn>>(interpret::Condition {
                op: ck,
                value: args::parse_condition(cs.as_str()).map_err(|error| {
                    error.map_location(|offset| {
                        loc::LineColumn::from_offset2(loc::Origin::CommandLine(ck), offset, cs)
                    })
                })?,
            })
        })
        .transpose()?;
    let source_directory = match args.script.parent() {
        None => std::path::Path::new("."),
        Some(parent) => parent,
    };
    let jobs = args.jobs.unwrap_or(1);
    let execute_semaphore = async_semaphore::Semaphore::new(jobs);
    let context = interpret::Context {
        system: ConcreteSystem {},
        functions: functions::map(),
        condition,
        execute_semaphore,
    };
    let env = interpret::Env {
        source_directory: std::path::PathBuf::from(source_directory),
    };
    let dict = indexmap::IndexMap::new();
    let rt = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()?;
    rt.block_on(interpret::execute_string(
        context,
        act,
        env,
        dict,
        content.clone(),
    ))
    .map_err(|error| {
        error.map_location(|offset| {
            let offset = match offset {
                loc::Offset::EndOfFile => content.len(),
                loc::Offset::Offset(offset) => offset,
            };
            loc::LineColumn::from_offset(loc::Origin::File(filename.clone()), offset, &content)
        })
    })
}

fn execute(args: args::Cli) -> Result<(), error::Error<loc::LineColumn>> {
    let content =
        std::fs::read_to_string(&args.script).map_err(|error| error::Error::CouldNotReadFile {
            filename: args.script.clone(),
            error,
        })?;
    let failures = std::sync::Arc::new(std::sync::Mutex::new(0));
    let act = MainAct {
        print_combinations: args.print_combinations,
        exec: args.exec.clone(),
        failures: failures.clone(),
    };
    execute_content(act, &args, content)?;
    let failures = *std::sync::Arc::try_unwrap(failures)
        .unwrap()
        .lock()
        .unwrap();
    if failures == 0 {
        Ok(())
    } else {
        Err(error::Error::ActionFailed(failures))
    }
}

fn main() {
    let args = args::Cli::parse();
    match execute(args) {
        Ok(()) => (),
        Err(msg) => {
            println!("Error: {}", msg);
            std::process::exit(1);
        }
    }
}

#[cfg(test)]
mod tests {
    use indexmap::IndexMap;

    use crate::{args, ast, error, functions, interpret, loc};
    use clap::Parser;

    fn execute_in_vec<S: interpret::System>(
        system: S,
        content: &str,
    ) -> ast::Result<Vec<indexmap::IndexMap<ast::Var, ast::ConstantRef>>> {
        let context = interpret::Context {
            system,
            functions: functions::map(),
            condition: None,
            execute_semaphore: async_semaphore::Semaphore::new(1),
        };
        let env = interpret::Env {
            source_directory: std::path::PathBuf::new(),
        };
        let dict = indexmap::IndexMap::new();
        let act = interpret::ActList::new();
        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()?;
        rt.block_on(interpret::execute_string_raw(
            &context,
            &act,
            &env,
            dict,
            content,
            std::collections::VecDeque::new(),
        ))?;
        Ok(act.unwrap().unwrap())
    }

    struct EmptySystem {}

    impl interpret::System for EmptySystem {
        fn exists(&self, _path: &std::path::Path) -> bool {
            false
        }

        fn rename(&self, _src: &std::path::Path, _tgt: &std::path::Path) -> std::io::Result<()> {
            Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "empty system",
            ))
        }

        fn reader(&self, _path: &std::path::Path) -> std::io::Result<String> {
            Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "empty system",
            ))
        }

        fn writer(&self, _path: &std::path::Path, _contents: &str) -> std::io::Result<()> {
            Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "empty system",
            ))
        }
    }

    #[test]
    fn x_eq_1() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(EmptySystem {}, "- x = 1;")?,
            vec![IndexMap::from([(
                ast::Var::from("x"),
                ast::ConstantRef::from(1)
            )])]
        );
        Ok(())
    }

    #[test]
    fn int_fail() -> ast::Result<()> {
        execute_in_vec(EmptySystem {}, "- x = 12345678901234567890;")
            .expect_err("Should fail (too large number)!");
        Ok(())
    }

    #[test]
    fn x_in_a_b() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(EmptySystem {}, "- x in ['a', 'b'];")?,
            vec![
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from("a"))]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from("b"))])
            ]
        );
        Ok(())
    }

    #[test]
    fn cartesian_product() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              - x in [1, 2];
              - y in ['a', 'b'];"
            )?,
            vec![
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(1)),
                    (ast::Var::from("y"), ast::ConstantRef::from("a"))
                ]),
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(1)),
                    (ast::Var::from("y"), ast::ConstantRef::from("b"))
                ]),
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(2)),
                    (ast::Var::from("y"), ast::ConstantRef::from("a"))
                ]),
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(2)),
                    (ast::Var::from("y"), ast::ConstantRef::from("b"))
                ])
            ]
        );
        Ok(())
    }

    #[test]
    fn scalar_product() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * linked:
                - x in [1, 2];
                - y in ['a', 'b'];"
            )?,
            vec![
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(1)),
                    (ast::Var::from("y"), ast::ConstantRef::from("a"))
                ]),
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(2)),
                    (ast::Var::from("y"), ast::ConstantRef::from("b"))
                ])
            ]
        );
        Ok(())
    }

    #[test]
    fn conditional() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x in [1, 2];
              * if x = 1:
                - y = 'a';"
            )?,
            vec![
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(1)),
                    (ast::Var::from("y"), ast::ConstantRef::from("a"))
                ]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(2))])
            ]
        );
        Ok(())
    }

    #[test]
    fn add() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x = 1;
              * y = x - 2 + 1;
              * z = 'a' + 'b';
              * t = 0.1 + 2;"
            )?,
            vec![IndexMap::from([
                (ast::Var::from("x"), ast::ConstantRef::from(1)),
                (ast::Var::from("y"), ast::ConstantRef::from(0)),
                (ast::Var::from("z"), ast::ConstantRef::from("ab")),
                (ast::Var::from("t"), ast::ConstantRef::from(2.1))
            ]),]
        );
        Ok(())
    }

    #[test]
    fn mul() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x = 1;
              * y = x * 4;
              * z = y / 0.5 * 3;"
            )?,
            vec![IndexMap::from([
                (ast::Var::from("x"), ast::ConstantRef::from(1)),
                (ast::Var::from("y"), ast::ConstantRef::from(4)),
                (ast::Var::from("z"), ast::ConstantRef::from(24.))
            ])]
        );
        Ok(())
    }

    #[test]
    fn unary_minus() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x = -1;"
            )?,
            vec![IndexMap::from([(
                ast::Var::from("x"),
                ast::ConstantRef::from(-1)
            )])]
        );
        Ok(())
    }

    #[test]
    fn regex() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x in ['foo', 'bar'];
              * if x ~= /a/:
                - y = 'a';"
            )?,
            vec![
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from("foo"))]),
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from("bar")),
                    (ast::Var::from("y"), ast::ConstantRef::from("a"))
                ])
            ]
        );
        Ok(())
    }

    #[test]
    fn regex_fail() -> ast::Result<()> {
        execute_in_vec(
            EmptySystem {},
            "
            * x in ['foo', 'bar'];
            * if x ~= /[a/:
              - y = 'a';",
        )
        .expect_err("Should fail (syntax error)!");
        Ok(())
    }

    #[test]
    fn type_fun() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x in [0, 0.5, 'bar'];
              * if type(x) = float:
                - m = 'float!';"
            )?,
            vec![
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(0))]),
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(0.5)),
                    (ast::Var::from("m"), ast::ConstantRef::from("float!"))
                ]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from("bar"))])
            ]
        );
        Ok(())
    }

    #[test]
    fn str() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x in [0, 0.5, 'bar'];
              * y = str(x);"
            )?,
            vec![
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(0)),
                    (ast::Var::from("y"), ast::ConstantRef::from("0"))
                ]),
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(0.5)),
                    (ast::Var::from("y"), ast::ConstantRef::from("0.5"))
                ]),
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from("bar")),
                    (ast::Var::from("y"), ast::ConstantRef::from("bar"))
                ])
            ]
        );
        Ok(())
    }

    #[test]
    fn and_or() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x in [0, 2];
              * y = x != 0 and 4 / x;
              * z = x = 0 or 4 / x;"
            )?,
            vec![
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(0)),
                    (ast::Var::from("y"), ast::ConstantRef::from(false)),
                    (ast::Var::from("z"), ast::ConstantRef::from(true))
                ]),
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(2)),
                    (ast::Var::from("y"), ast::ConstantRef::from(2)),
                    (ast::Var::from("z"), ast::ConstantRef::from(2))
                ])
            ]
        );
        Ok(())
    }

    #[test]
    fn defined() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x = defined(y);
              * y = defined(x);"
            )?,
            vec![IndexMap::from([
                (ast::Var::from("x"), ast::ConstantRef::from(false)),
                (ast::Var::from("y"), ast::ConstantRef::from(true))
            ])]
        );
        Ok(())
    }

    #[test]
    fn in_list() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x in [[1, 2], [3, 4]];
              * y in x;"
            )?,
            vec![
                IndexMap::from([
                    (
                        ast::Var::from("x"),
                        ast::ConstantRef::from(vec![
                            ast::ConstantRef::from(1),
                            ast::ConstantRef::from(2)
                        ])
                    ),
                    (ast::Var::from("y"), ast::ConstantRef::from(1))
                ]),
                IndexMap::from([
                    (
                        ast::Var::from("x"),
                        ast::ConstantRef::from(vec![
                            ast::ConstantRef::from(1),
                            ast::ConstantRef::from(2)
                        ])
                    ),
                    (ast::Var::from("y"), ast::ConstantRef::from(2))
                ]),
                IndexMap::from([
                    (
                        ast::Var::from("x"),
                        ast::ConstantRef::from(vec![
                            ast::ConstantRef::from(3),
                            ast::ConstantRef::from(4)
                        ])
                    ),
                    (ast::Var::from("y"), ast::ConstantRef::from(3))
                ]),
                IndexMap::from([
                    (
                        ast::Var::from("x"),
                        ast::ConstantRef::from(vec![
                            ast::ConstantRef::from(3),
                            ast::ConstantRef::from(4)
                        ])
                    ),
                    (ast::Var::from("y"), ast::ConstantRef::from(4))
                ])
            ]
        );
        Ok(())
    }

    struct IncludeSystem {}

    impl interpret::System for IncludeSystem {
        fn exists(&self, _path: &std::path::Path) -> bool {
            false
        }

        fn rename(&self, _src: &std::path::Path, _tgt: &std::path::Path) -> std::io::Result<()> {
            Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "empty system",
            ))
        }

        fn reader(&self, path: &std::path::Path) -> std::io::Result<String> {
            match path.to_str().unwrap() {
                "test1.prune" => Ok(String::from("* x in [0,1];")),
                "dir1/test2.prune" => Ok(String::from(
                    "* include path([source_directory(), 'dir2/test3.prune']);",
                )),
                "dir1/dir2/test3.prune" => Ok(String::from("* x in [0,1];")),
                _ => Err(std::io::Error::from(std::io::ErrorKind::NotFound)),
            }
        }

        fn writer(&self, _path: &std::path::Path, _contents: &str) -> std::io::Result<()> {
            Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "empty system",
            ))
        }
    }

    #[test]
    fn include1() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                IncludeSystem {},
                "
              * include path([source_directory(), 'test1.prune']);"
            )?,
            vec![
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(0))]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(1))])
            ]
        );
        Ok(())
    }

    #[test]
    fn include2() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                IncludeSystem {},
                "
              * include path([source_directory(), 'dir1/test2.prune']);"
            )?,
            vec![
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(0))]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(1))])
            ]
        );
        Ok(())
    }

    #[test]
    fn range() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x in range(0,3);"
            )?,
            vec![
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(0))]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(1))]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(2))])
            ]
        );
        Ok(())
    }

    #[test]
    fn range_step() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x in range(0,4,2);"
            )?,
            vec![
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(0))]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(2))])
            ]
        );
        Ok(())
    }

    #[test]
    fn map_left() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x in range(0, 4) * 0.5;"
            )?,
            vec![
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(0.0))]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(0.5))]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(1.0))]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(1.5))])
            ]
        );
        Ok(())
    }

    #[test]
    fn map_right() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x in 0.5 * range(0, 4);"
            )?,
            vec![
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(0.0))]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(0.5))]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(1.0))]),
                IndexMap::from([(ast::Var::from("x"), ast::ConstantRef::from(1.5))])
            ]
        );
        Ok(())
    }

    #[test]
    fn dict() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * d = { a: 1, b: 2 };
              * a = d.a;
              * b = d.b;"
            )?,
            vec![IndexMap::from([
                (
                    ast::Var::from("d"),
                    ast::ConstantRef::from(indexmap::IndexMap::from([
                        ("a".to_string(), ast::ConstantRef::from(1)),
                        ("b".to_string(), ast::ConstantRef::from(2)),
                    ]))
                ),
                (ast::Var::from("a"), ast::ConstantRef::from(1)),
                (ast::Var::from("b"), ast::ConstantRef::from(2))
            ])]
        );
        Ok(())
    }

    #[test]
    fn dict_list() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * d = [{ a: 1, b: 2 }, { a : 3 }, { b : 4 }];
              * a = d.a;"
            )?,
            vec![IndexMap::from([
                (
                    ast::Var::from("d"),
                    ast::ConstantRef::from(vec![
                        ast::ConstantRef::from(indexmap::IndexMap::from([
                            ("a".to_string(), ast::ConstantRef::from(1)),
                            ("b".to_string(), ast::ConstantRef::from(2)),
                        ])),
                        ast::ConstantRef::from(indexmap::IndexMap::from([(
                            "a".to_string(),
                            ast::ConstantRef::from(3)
                        ),])),
                        ast::ConstantRef::from(indexmap::IndexMap::from([(
                            "b".to_string(),
                            ast::ConstantRef::from(4)
                        ),])),
                    ])
                ),
                (
                    ast::Var::from("a"),
                    ast::ConstantRef::from(vec![
                        ast::ConstantRef::from(1),
                        ast::ConstantRef::from(3)
                    ])
                )
            ])]
        );
        Ok(())
    }

    #[test]
    fn collect() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x in [1, 2];
              * collect l:
                - u = x;
                - v in ['a', 'b'];
              * d in l;"
            )?,
            vec![
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(1)),
                    (
                        ast::Var::from("l"),
                        ast::ConstantRef::from(vec![
                            ast::ConstantRef::from(indexmap::IndexMap::from([
                                ("u".to_string(), ast::ConstantRef::from(1)),
                                ("v".to_string(), ast::ConstantRef::from("a".to_string()))
                            ])),
                            ast::ConstantRef::from(indexmap::IndexMap::from([
                                ("u".to_string(), ast::ConstantRef::from(1)),
                                ("v".to_string(), ast::ConstantRef::from("b".to_string()))
                            ]))
                        ])
                    ),
                    (
                        ast::Var::from("d"),
                        ast::ConstantRef::from(indexmap::IndexMap::from([
                            ("u".to_string(), ast::ConstantRef::from(1)),
                            ("v".to_string(), ast::ConstantRef::from("a".to_string()))
                        ]))
                    )
                ]),
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(1)),
                    (
                        ast::Var::from("l"),
                        ast::ConstantRef::from(vec![
                            ast::ConstantRef::from(indexmap::IndexMap::from([
                                ("u".to_string(), ast::ConstantRef::from(1)),
                                ("v".to_string(), ast::ConstantRef::from("a".to_string()))
                            ])),
                            ast::ConstantRef::from(indexmap::IndexMap::from([
                                ("u".to_string(), ast::ConstantRef::from(1)),
                                ("v".to_string(), ast::ConstantRef::from("b".to_string()))
                            ]))
                        ])
                    ),
                    (
                        ast::Var::from("d"),
                        ast::ConstantRef::from(indexmap::IndexMap::from([
                            ("u".to_string(), ast::ConstantRef::from(1)),
                            ("v".to_string(), ast::ConstantRef::from("b".to_string()))
                        ]))
                    )
                ]),
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(2)),
                    (
                        ast::Var::from("l"),
                        ast::ConstantRef::from(vec![
                            ast::ConstantRef::from(indexmap::IndexMap::from([
                                ("u".to_string(), ast::ConstantRef::from(2)),
                                ("v".to_string(), ast::ConstantRef::from("a".to_string()))
                            ])),
                            ast::ConstantRef::from(indexmap::IndexMap::from([
                                ("u".to_string(), ast::ConstantRef::from(2)),
                                ("v".to_string(), ast::ConstantRef::from("b".to_string()))
                            ]))
                        ])
                    ),
                    (
                        ast::Var::from("d"),
                        ast::ConstantRef::from(indexmap::IndexMap::from([
                            ("u".to_string(), ast::ConstantRef::from(2)),
                            ("v".to_string(), ast::ConstantRef::from("a".to_string()))
                        ]))
                    )
                ]),
                IndexMap::from([
                    (ast::Var::from("x"), ast::ConstantRef::from(2)),
                    (
                        ast::Var::from("l"),
                        ast::ConstantRef::from(vec![
                            ast::ConstantRef::from(indexmap::IndexMap::from([
                                ("u".to_string(), ast::ConstantRef::from(2)),
                                ("v".to_string(), ast::ConstantRef::from("a".to_string()))
                            ])),
                            ast::ConstantRef::from(indexmap::IndexMap::from([
                                ("u".to_string(), ast::ConstantRef::from(2)),
                                ("v".to_string(), ast::ConstantRef::from("b".to_string()))
                            ]))
                        ])
                    ),
                    (
                        ast::Var::from("d"),
                        ast::ConstantRef::from(indexmap::IndexMap::from([
                            ("u".to_string(), ast::ConstantRef::from(2)),
                            ("v".to_string(), ast::ConstantRef::from("b".to_string()))
                        ]))
                    )
                ])
            ]
        );
        Ok(())
    }

    #[test]
    fn execute_hello() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * execute('echo Hello');"
            )?,
            vec![IndexMap::new()]
        );
        Ok(())
    }

    #[test]
    fn list() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * r = range(1, 5);
              * l = len(r);
              * v = r[3];"
            )?,
            vec![IndexMap::from([
                (
                    ast::Var::from("r"),
                    ast::ConstantRef::from(vec![
                        ast::ConstantRef::from(1),
                        ast::ConstantRef::from(2),
                        ast::ConstantRef::from(3),
                        ast::ConstantRef::from(4),
                    ])
                ),
                (ast::Var::from("l"), ast::ConstantRef::from(4)),
                (ast::Var::from("v"), ast::ConstantRef::from(4))
            ])]
        );
        Ok(())
    }

    #[test]
    fn list_fail() -> ast::Result<()> {
        execute_in_vec(EmptySystem {}, "- i = range(1, 10)[9];")
            .expect_err("Should fail (out of bounds)!");
        Ok(())
    }

    #[test]
    fn if_value() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * b in [false, true];
              * if b:
                - x = 1;
              * y = if defined(x) then x else 0;"
            )?,
            vec![
                IndexMap::from([
                    (ast::Var::from("b"), ast::ConstantRef::from(false)),
                    (ast::Var::from("y"), ast::ConstantRef::from(0))
                ]),
                IndexMap::from([
                    (ast::Var::from("b"), ast::ConstantRef::from(true)),
                    (ast::Var::from("x"), ast::ConstantRef::from(1)),
                    (ast::Var::from("y"), ast::ConstantRef::from(1))
                ])
            ]
        );
        Ok(())
    }

    #[test]
    fn syntactic_range() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x = [0 .. 4];
              * y = [5, 4 .. 1];
              * z = [1, 1.5 .. 3];"
            )?,
            vec![IndexMap::from([
                (
                    ast::Var::from("x"),
                    ast::ConstantRef::from(vec![0, 1, 2, 3, 4])
                ),
                (
                    ast::Var::from("y"),
                    ast::ConstantRef::from(vec![5, 4, 3, 2, 1])
                ),
                (
                    ast::Var::from("z"),
                    ast::ConstantRef::from(vec![1.0, 1.5, 2.0, 2.5, 3.0])
                )
            ])]
        );
        Ok(())
    }

    #[test]
    fn syntactic_range_map() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * l = [0, .., 8];
              * m = [str(l[0] + 2) .. str(l[len(l) - 1] + 2)];
              * n = [l[1], l[3], .., l[len(l) - 1]];"
            )?,
            vec![IndexMap::from([
                (
                    ast::Var::from("l"),
                    ast::ConstantRef::from(vec![0, 1, 2, 3, 4, 5, 6, 7, 8])
                ),
                (
                    ast::Var::from("m"),
                    ast::ConstantRef::from(vec!["2", "3", "4", "5", "6", "7", "8", "9", "10"])
                ),
                (
                    ast::Var::from("n"),
                    ast::ConstantRef::from(vec![1, 3, 5, 7])
                )
            ])]
        );
        Ok(())
    }

    #[test]
    fn syntactic_fold() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x = 0 + 1 + 2;
              * y = 1 + .. + 4;
              * z = 1 + 3 + .. + 9;
              * l = [1, 2, 3, 4, 5, 9];
              * m = (l[0] + .. + l[len(l) - 1]) / len(l);
              * l = [false, false, true, false];
              * b = l[0] or .. or l[len(l) - 1];
              * c = l[0] and .. and l[len(l) - 1];
              * l = [];
              * zero = l[0] + .. + l[len(l) - 1];
              * one = l[0] * .. * l[len(l) - 1];
              * f = l[0] or .. or l[len(l) - 1];
              * t = l[0] and .. and l[len(l) - 1];
              * l = ['a', 'b', 'c'];
              * abc = '' + l[0] + .. + l[len(l) - 1];
              * u = 1 - 2 - 3 - 4 - .. - 2; # reduced to 1 - 2
              * v = 1 - .. - 0 - 2 - 3; # reduced to 2 - 3
                  "
            )?,
            vec![IndexMap::from([
                (ast::Var::from("x"), ast::ConstantRef::from(3)),
                (ast::Var::from("y"), ast::ConstantRef::from(10)),
                (ast::Var::from("z"), ast::ConstantRef::from(25)),
                (
                    ast::Var::from("l"),
                    ast::ConstantRef::from(vec![1, 2, 3, 4, 5, 9])
                ),
                (ast::Var::from("m"), ast::ConstantRef::from(4)),
                (ast::Var::from("m"), ast::ConstantRef::from(4)),
                (ast::Var::from("b"), ast::ConstantRef::from(true)),
                (ast::Var::from("c"), ast::ConstantRef::from(false)),
                (ast::Var::from("l"), ast::ConstantRef::empty_list()),
                (ast::Var::from("zero"), ast::ConstantRef::from(0)),
                (ast::Var::from("one"), ast::ConstantRef::from(1)),
                (ast::Var::from("f"), ast::ConstantRef::from(false)),
                (ast::Var::from("t"), ast::ConstantRef::from(true)),
                (
                    ast::Var::from("l"),
                    ast::ConstantRef::from(vec!["a", "b", "c"])
                ),
                (ast::Var::from("abc"), ast::ConstantRef::from("abc")),
                (ast::Var::from("u"), ast::ConstantRef::from(-1)),
                (ast::Var::from("v"), ast::ConstantRef::from(-1))
            ])]
        );
        Ok(())
    }

    #[test]
    fn to_json_obj() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * dic = {a: 1, b: ['tut', 'pouet'], c: {k1: 10, k2: 11}};
              * d = to_json(dic);"
            )?,
            vec![IndexMap::from([
                (
                    ast::Var::from("dic"),
                    ast::ConstantRef::from(indexmap::IndexMap::from([
                        ("a".to_string(), ast::ConstantRef::from(1)),
                        (
                            "b".to_string(),
                            ast::ConstantRef::from(vec!["tut", "pouet"])
                        ),
                        (
                            "c".to_string(),
                            ast::ConstantRef::from(indexmap::IndexMap::from([
                                ("k1".to_string(), ast::ConstantRef::from(10)),
                                ("k2".to_string(), ast::ConstantRef::from(11))
                            ]))
                        ),
                    ]))
                ),
                (
                    ast::Var::from("d"),
                    ast::ConstantRef::from(r#"{"a":1,"b":["tut","pouet"],"c":{"k1":10,"k2":11}}"#)
                )
            ])]
        );
        Ok(())
    }

    #[test]
    fn to_json_array() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * arr = [1, 'A'];
              * d = to_json(arr);"
            )?,
            vec![IndexMap::from([
                (
                    ast::Var::from("arr"),
                    ast::ConstantRef::from(ast::ConstantRef::from(vec![
                        ast::ConstantRef::from(1),
                        ast::ConstantRef::from("A")
                    ]))
                ),
                (ast::Var::from("d"), ast::ConstantRef::from(r#"[1,"A"]"#))
            ])]
        );
        Ok(())
    }
    #[test]
    fn from_json_obj() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * dic = {a: 1, b: ['tut', 'pouet'], c: {k1: 10, k2: 11}};
              * d = from_json(to_json(dic));
              * res = if d = dic then 0 else 1;
          "
            )?,
            vec![IndexMap::from([
                (
                    ast::Var::from("dic"),
                    ast::ConstantRef::from(indexmap::IndexMap::from([
                        ("a".to_string(), ast::ConstantRef::from(1)),
                        (
                            "b".to_string(),
                            ast::ConstantRef::from(vec!["tut", "pouet"])
                        ),
                        (
                            "c".to_string(),
                            ast::ConstantRef::from(indexmap::IndexMap::from([
                                ("k1".to_string(), ast::ConstantRef::from(10)),
                                ("k2".to_string(), ast::ConstantRef::from(11))
                            ]))
                        ),
                    ]))
                ),
                (
                    ast::Var::from("d"),
                    ast::ConstantRef::from(indexmap::IndexMap::from([
                        ("a".to_string(), ast::ConstantRef::from(1)),
                        (
                            "b".to_string(),
                            ast::ConstantRef::from(vec!["tut", "pouet"])
                        ),
                        (
                            "c".to_string(),
                            ast::ConstantRef::from(indexmap::IndexMap::from([
                                ("k1".to_string(), ast::ConstantRef::from(10)),
                                ("k2".to_string(), ast::ConstantRef::from(11))
                            ]))
                        ),
                    ]))
                ),
                (ast::Var::from("res"), ast::ConstantRef::from(0))
            ])]
        );
        Ok(())
    }

    #[test]
    fn from_json_array() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * arr = [1, 'A'];
              * a = from_json(to_json(arr));
              * res = if a = arr then 0 else 1;
          "
            )?,
            vec![IndexMap::from([
                (
                    ast::Var::from("arr"),
                    ast::ConstantRef::from(ast::ConstantRef::from(vec![
                        ast::ConstantRef::from(1),
                        ast::ConstantRef::from("A")
                    ]))
                ),
                (
                    ast::Var::from("a"),
                    ast::ConstantRef::from(ast::ConstantRef::from(vec![
                        ast::ConstantRef::from(1),
                        ast::ConstantRef::from("A")
                    ]))
                ),
                (ast::Var::from("res"), ast::ConstantRef::from(0))
            ])]
        );
        Ok(())
    }

    #[test]
    fn sha256() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x = hash('pouet', 'sha256');"
            )?,
            execute_in_vec(
                EmptySystem {},
                "
              * x = '7ED8EDA08E2D4A11A5459CC3453F54171591C0A39A113EAACC1F421DEB5A9792';"
            )?,
        );
        Ok(())
    }

    #[test]
    fn assoc() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * x = 'a' + 'b' + 'c' + 'd';"
            )?,
            execute_in_vec(
                EmptySystem {},
                "
              * x = 'abcd';"
            )?,
        );
        Ok(())
    }

    #[test]
    fn defined_field() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * d = { f : '1' };
              * x = defined(d.f);
              * y = defined(d.g);"
            )?,
            execute_in_vec(
                EmptySystem {},
                "
              * d = { f : '1' };
              * x = true;
              * y = false;"
            )?,
        );
        Ok(())
    }

    #[test]
    fn configure() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * fruit = 'Prune';
              * x = configure('${fruit} juice is laxative.');"
            )?,
            execute_in_vec(
                EmptySystem {},
                "
              * fruit = 'Prune';
              * x = 'Prune juice is laxative.';"
            )?,
        );
        Ok(())
    }

    #[test]
    fn configure_non_var() -> ast::Result<()> {
        execute_in_vec(
            EmptySystem {},
            "
              * vegetable = 'Carrot';
              * x = configure('${fruit} juice is laxative.');",
        )
        .expect_err("Should fail (unbound variable)!");
        Ok(())
    }

    #[test]
    fn configure_embed_var() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * purple_fruit = 'Prune';
              * color = 'purple';
              * x = configure('${${color}_fruit} juice is laxative.');",
            )?,
            execute_in_vec(
                EmptySystem {},
                "
              * color = 'purple';
              * purple_fruit = 'Prune';
              * x = 'Prune juice is laxative.';"
            )?
        );
        Ok(())
    }

    #[test]
    fn configure_curly_brackets() -> ast::Result<()> {
        assert_eq!(
            execute_in_vec(
                EmptySystem {},
                "
              * value = 10;
              * x = configure('{\"A\" = ${value}}');",
            )?,
            execute_in_vec(
                EmptySystem {},
                "
              * value = 10;
              * x = '{\"A\" = 10}';"
            )?
        );
        Ok(())
    }

    fn execute_content_in_vec<S: interpret::System>(
        system: S,
        args: &args::Cli,
        content: &str,
    ) -> Result<Vec<indexmap::IndexMap<ast::Var, ast::ConstantRef>>, error::Error<loc::LineColumn>>
    {
        let (cond_kind, cond_str) = match (&args.skip, &args.keep) {
            (None, None) => (None, None),
            (Some(s), None) => (Some(interpret::ConditionKind::Skip), Some(s)),
            (None, Some(k)) => (Some(interpret::ConditionKind::Keep), Some(k)),
            (Some(_), Some(_)) => Err(error::Error::CommandLine(
                error::CommandLineError::BothSkipAndKeep,
            ))?,
        };
        let condition = match (cond_kind, &cond_str) {
            (Some(ck), Some(cs)) => Some(interpret::Condition {
                op: ck,
                value: args::parse_condition(cs.as_str()).map_err(|error| {
                    error.map_location(|offset| {
                        loc::LineColumn::from_offset2(loc::Origin::CommandLine(ck), offset, cs)
                    })
                })?,
            }),
            _ => None,
        };
        let execute_semaphore = async_semaphore::Semaphore::new(1);
        let context = interpret::Context {
            system,
            functions: functions::map(),
            condition: condition,
            execute_semaphore,
        };
        let env = interpret::Env {
            source_directory: std::path::PathBuf::new(),
        };
        let dict = indexmap::IndexMap::new();
        let act = interpret::ActList::new();
        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()?;
        rt.block_on(interpret::execute_string_raw(
            &context,
            &act,
            &env,
            dict,
            content,
            std::collections::VecDeque::new(),
        ))
        .map_err(|error| {
            error.map_location(|offset| {
                let offset = match offset {
                    loc::Offset::EndOfFile => content.len(),
                    loc::Offset::Offset(offset) => offset,
                };
                loc::LineColumn::from_offset(
                    loc::Origin::File(std::path::PathBuf::from("test")),
                    offset,
                    &content,
                )
            })
        })?;
        Ok(act.unwrap().unwrap())
    }
    #[test]
    fn skip() -> Result<(), error::Error<loc::LineColumn>> {
        assert_eq!(
            execute_content_in_vec(
                EmptySystem {},
                &args::Cli::parse_from(["", "dumb_name", "-s a=1"]),
                "
            * a in [1, 2];
            * b in ['b1', 'b2'];"
            )?,
            execute_content_in_vec(
                EmptySystem {},
                &args::Cli::parse_from(["", "dumb_name"]),
                "
            * a = 2;
            * b in ['b1', 'b2'];"
            )?,
        );
        Ok(())
    }
    #[test]
    fn keep() -> Result<(), error::Error<loc::LineColumn>> {
        assert_eq!(
            execute_content_in_vec(
                EmptySystem {},
                &args::Cli::parse_from(["", "dumb_name", "-k a=1"]),
                "
            * a in [1, 2];
            * b in ['b1', 'b2'];"
            )?,
            execute_content_in_vec(
                EmptySystem {},
                &args::Cli::parse_from(["", "dumb_name"]),
                "
            * a = 1;
            * b in ['b1', 'b2'];"
            )?,
        );
        Ok(())
    }
}
