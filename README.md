# Prune in Rust

`Pruners` generates parameter sets from a specification file.
The generated parameter sets can either be printed or used to execute commands.

## Installation

`Pruners` requires a recent version of `rust`:
depending on your system, you may run `apt install cargo` or equivalent,
or install [rustup](https://www.rust-lang.org/tools/install).

To install `Pruners`, run the following command in the repository.
```
cargo install --path .
```

The tool `Pruners` is installed in `~/.cargo/bin`.
Please make sure that this directory is in your path.

## Basic usage

Given the following specification file `example.prune`,
```
* x in ["a", "b"];
* y in [1, 2];
* if x = "a" and y = 1:
  - z in [0.1, 0.2, 0.3];
  - linked:
    -- a in ["u", "v"];
    -- b in [true, false];
```
the command
```
pruners -p example.prune
```
prints the following output.
```
{x = "a", y = 1, z = 0.1, a = "u", b = true}
{x = "a", y = 1, z = 0.1, a = "v", b = false}
{x = "a", y = 1, z = 0.2, a = "u", b = true}
{x = "a", y = 1, z = 0.2, a = "v", b = false}
{x = "a", y = 1, z = 0.3, a = "u", b = true}
{x = "a", y = 1, z = 0.3, a = "v", b = false}
{x = "a", y = 2}
{x = "b", y = 1}
{x = "b", y = 2}
```

A shell command can be executed for every parameter set by using command-line option `-e`.
For instance,
```
pruners example.prune -e 'echo -$x- -$y- -$z- -$a- -$b-'
```
prints the following output.
```
-a- -1- -0.1- -u- -true-
-a- -1- -0.1- -v- -false-
-a- -1- -0.2- -u- -true-
-a- -1- -0.2- -v- -false-
-a- -1- -0.3- -u- -true-
-a- -1- -0.3- -v- -false-
-a- -2- -- -- --
-b- -1- -- -- --
-b- -2- -- -- --
```

## Specification file

A specification file is a hierarchical list of rules.
Each rule begins with a bullet
(a sequence of characters among `*`, `+` and `-`)
that specifies how rules are nested with each other
(rules with identical bullets are on the same level).
Rules end with semi-colon (`;`).

A rule can have one of the following form.
- an assignment: `- x = 1;`
- an enumeration: `- x in [1, 2, 3];`
- a condition: `- if x > 2:`, followed by at least one sub-rule
- a list of linked variables: `- linked:`,
  followed by at least one sub-enumeration.
- a file inclusion: `- include "file/path";`
- a function call with a unit (void) result: `- write("file/path", "content");`
- a collection of all the combinations of a set of sub-rules `- collect l:`

Linked variables are associated one-to-one (like a scalar product),
whereas other enumerations are expanded for all possible combinations
(like a Cartesian product).

Variables begin with a letter (upper or lowercase) or a `_`, followed
by letters, `_` or digits (for instance, `_Abc123K` is a valid variable name).

Values can have one of the following form.
- a constant (a 64-bit integer, a 64-bit float, strings,
  Boolean `true` or `false`,
  type names `bool`, `int`, `float`, `string`, `type`, `list`, `dict` and `unit`),
- a reference to a variable previously bound,
- a list of values `[1, 2, 3]`,
- a dictionary of values `{ a: 1, b : 2 }`,
- basic arithmetic operations (`+`, `-`, `*`, `/`),
  comparisons (`=`, `!=`, `<`, `<=`, `>`, `>=`),
  and Boolean operations (`and`, `or`),
- string-matching against a regular expression (`x ~= /^a/`),
- projection over a dictionary field (`d.f`),
- indexing an element of a list (`l[i]`), counting from zero,
- application of some predefined functions,
- conditional values (`if c then value_if_true else value_if_false`),
- range construction or operation (`[a .. b]` or `a + .. + b`).

In an enumeration rule `x in l`, `l` should evaluate to a list.
In a condition rule `if c:`, `c` should evaluate to a Boolean.

In a list of linked variables, all expressions at the right-hand side
of `in` should evaluate to lists of the same length. Lists are
evaluated at once, so linked variables cannot reference to each other.
For instance, the following definition is invalid,
```
* linked:
  - x in [1, 2];
  - y in [3, x]; # unbound reference to x
```
whereas the same enumeration rules would have been valid outside `linked:`.

If a projection over a directionary field is applied to a list of dictionaries,
the list of projected fields is returned, ignoring the dictionaries that miss
this field. For instance, if `list = [{a:1,b:2},{a:2},{b:3}]`, we have
`list.a = [1,2]`.

Predefined functions are the following.
- `not(b)` returns `false` if `x` is `true`, `true` if `x` is `false`
- `defined(x)` returns `true` if `x` was previously bound, `false` otherwise
  (`x` is required to be a variable),
- `type(x)` returns the type of `x`, `type(type(x))` is always `type`,
- `str(x)` returns a string representation of `x`
  (if `x` is a string, it is the identity).
- `range(start,end)` returns a list of integers between start (included) and
  end (excluded). `range(start,end,step)` returns a list of integers between 
  start (included) and end (excluded), separated by step.
- `len(l)` returns the length of the list `l`.
- `read("file/path")` returns the content of a file as a string.
- `write("file/path", "content")` writes the content to a file and returns unit (void).
- `exists("file/path")` returns `true` if the file exists, `false` otherwise.
- `rename("source/file/path", "target/file/path")` renames a file and returns unit (void);
  the command fails if the source and target are not in the same file system.
- `execute("shell command")` executes the given shell command and returns unit (void).
- `source_directory()` returns the path to the directory of the current source file.
- `path(["path_1", .., "path_n"])` concatenates the given paths.
- `to_json(val)` returns a [Json](https://www.json.org/json-en.html) formatted string
  from a value val of any type.
- `from_json(s)` returns a value of any type from a [Json](https://www.json.org/json-en.html)
  formatted string.
- `hash("content", "alg")`, returns a hash, as a string, of the string content using either
  sha256 or sha512 algorithms.
- `configure(s)`, substitutes `${foo}` in the string template `s` by the value of the
  `foo` variable if defined.

If a function is applied to a list whereas it does not expect a list,
the function is applied to all the elements of the list.
For instance, `read(["path1", "path2"])` returns the list containing two
elements, the content of `path1` and the content of `path2`.
This pattern works for operators as well: `[1, 2, 3] * 2`
is the list `[2, 4, 6]`.

Strings are delimited between simple quotes `'` or double quotes `"`.
The quote symbol can be escaped with `\`,
and the escape symbol `\` can be escaped with itself: `\\`.
Two strings can be concatenated with `+`.

Comments start with `#` and end at the end of the line.

## File inclusion

The functions `source_directory()` and `path()` are particularly
useful for file inclusion, since we generally want to include source
files with a path relative to the path of the current source file,
thanks to the following pattern.

```
- include path([source_directory(), "file.inc"]);
```

With this pattern, the path `file.inc` is considered as relative to
the path of the current source file (i.e, if it is just a filename
like `file.inc`, the file is searched in the same directory as the
current source file).

By opposition, the rule `- include "file.inc"` will consider the file
`file.inc` in the current working directory, making the behavior of
the rule dependent to the current working directory: if the current
source file is not in the current working directory (for instance, if
we run `prune` with a path such as `dir/file.prune`, or if the current
source file was included from another file in a different working
directory), the rule will not include the same file (or will fail
because the file does not exist).

## Conditional and default values

Conditional can be used in conjunction with the predefined function
`defined` to get default values when variables are not defined.
In the following example, `y` gets the value `0` in the branch `b =
false`, where `x` is not defined.

```
* b in [false, true];
* if b:
  - x = 1;
* y = if defined(x) then x else 0;
```

## Collections

A set of sub-rules can be grouped together to build the list of
dictionaries of all their combinations.

```
* collect x:
  - u in ['a', 'b'];
  - v in [1, 2];
```

In this example, `x` is bound to the list
`[{ u: 'a', v: 1}, { u: 'a', v: 2}, { u: 'b', v: 1}, { u: 'b', v: 2}]`.

## Ranges

Ranges can be constructed with the `..` notation.

```
- x in [ 0 .. 4 ];
```

`[ 0 .. 4 ]` (or `[ 0, .., 4]`) is a notation for the list
`[0, 1, 2, 3, 4]` (note that the two bounds are inclusive).

Default step is `1`.  The notation allows lists to be constructed with
another step by giving the two first values.  For instance,
`[ 1, 3 .. 9 ]` is a notation for the list `[1, 3, 5, 7, 9]`.
Note that `[ 1, 3 .. 10 ]` denotes the same list: `10` is excluded
because it is not reachable from `1` stepping two by two.

Ranges can be decreasing: `[5, 4, .., 0]` is a notation for the list
`[5, 4, 3, 2, 1]`.  Note that since the default step is `1`,
`[5 .. 0]` is the empty list.

Ranges can enumerate floats: in this case, specifying the step is
mandatory. For instance, `[1.5, 2.5, ..., 6.5]` is the list
`[1.5, 2.5, 3.5, 4.5, 5.5, 6.5]`.

Operations (and more generally, all binary functions) can be applied
over ranges: `1 + .. + 5` is interpreted as `1 + 2 + 3 + 4 + 5`.
This notation can be used to compute, for instance, the sum and the mean
of the elements of a list.

```
- l = [1 .. 10]
- sum = l[0] + .. + l[len(l) - 1]
- mean = (l[0] + .. + l[len(l) - 1]) / len(l)
```

As above, we can give the first two elements to step by something else than `1`,
for instance `l[1] + l[3] + ... + l[len(l) - 1]`.

This works for all other operators too: for instance, the following
example computes some binomial coefficients.

```
- n in [1 .. 5];
- k in [1 .. n];
- binom = (n * (n - 1) * .. * (n - k + 1)) / (k * (k - 1) * .. * 1);
```

On empty ranges, common operators return their neutral element when this makes sense.

```
* l = [];
* zero = l[0] + .. + l[len(l) - 1]; # zero = 0
* one = l[0] * .. * l[len(l) - 1]; # one = 1
* f = l[0] or .. or l[len(l) - 1]; # f = false
* t = l[0] and .. and l[len(l) - 1]; # t = true
```

For other operators or binary functions, this is an error to apply
them to empty lists, except if some initial element is given.

```
* l = [];
* x = 2 - l[0] - .. - l[len(l) - 1]; # x = 2
```

As noted above, the default initial element for `+` is `0`, making sums
fail if not applied to numbers. To concatenate strings, we can pass
explicitly the empty string as initial element (or final element).

```
* l = ['a', 'b', 'c'];
* abc = '' + l[0] + .. + l[len(l) - 1]; # abc = 'abc'
```

## Executing commands

There are two ways to execute commands in `prune`:

- either by using the `execute("shell command")` function,
- or by passing the option `-e "shell command"` to the command-line.

These two possibilities can be used together: the commands passed to
the `execute` function are executed during the evaluation of the
specification file, and can be followed by other rules and even other
commands to execute. The command passed through the command-line
option `-e` is executed at the end of the computation of each
parameter set: it is equivalent to add another `execute("shell
command")` rule at the end of the specification file, except that it
also disables the printing of the parameter set.

## Types

Prune types are organized in the following lattice. The arrow means
_is a subtype of_: for instance `int` is a subtype of `float`, meaning
that a value of type `int` can be used everywhere a value of type
`float` is required.

```mermaid
graph BT;
  empty-->unit;
  empty-->bool;
  empty-->int;
  empty-->string;
  empty-->list;
  empty-->dict;
  empty-->type;
  int-->float;
  unit-->any;
  bool-->any;
  float-->any;
  string-->any;
  list-->any;
  dict-->any;
  type-->any;
```

- `empty` type has no value: therefore, all values of type `empty`
  (there is none!) can be used everywhere a value of any type is
  required (_ex falso quodlibet_), that is why `empty` is a subtype of
  all other types. `empty` is the type of the elements of the empty
  list, and is the type of functions that do not return at all (there
  is no such function at this time in `prune`, but that would be the
  type of a function `exit` or `error`, or a function that performs an
  infinite loop).

- `unit` type has only one value, usually denoted `()` (even if there
  is no syntax for denoting the unit value in `prune` at this time):
  it can be seen as the type of the "argument" of functions that take
  no arguments (in the expression `f()`, we can consider that `f`
  takes one argument, `()`, but since there is only one possible value
  for it, `f` cannot make anything useful with this argument), and it
  is the type of the "result" of functions that returns with no return
  value (we can consider that they return the value `()`, which we
  cannot do anything useful from).

- `int` is a subtype of `float`, meaning that a value of type `int`
  can be used everywhere a value of type `float` is required, and that
  if a list contains a mix of `int` and `float` values, it can be
  considered as a list of `float` values.

- `any` is a supertype of all other types: a list that mix values of
  distinct types that are not subtypes of each other will be
  considered as a list of values of type `any`.

## Bullets

Bullets can be any sequence of characters among `*`, `+` and `-`
and they specify how rules are nested with each other:
indentation and new lines are ignored.

Bullets should change at each level
and cannot be identical to any ancestor.
Conditions and list of linked variables are enforced to have
at least one subrule to prevent structural errors.
For instance, the following specification file is rejected
```
* if true:
  - linked:
    * x in ["a", "b"];
```
because it is interpreted as
```
* if true:
  - linked:
* x in ["a", "b"];
```
and the list of linked variables would be empty.

## Contributing

Rust code is formatted with `cargo fmt`. You can use a [`pre-commit`]
hook to ensure this. `pre-commit` can be installed via `pip install
pre-commit` or `brew install pre-commit` or `conda install -c
conda-forge pre-commit`.

[`pre-commit`]: https://pre-commit.com/

Then, to initialize `pre-commit`, run the following command in the repository.

```
pre-commit install
```

`cargo fmt` and `cargo-check` will be executed automatically at each
commit.
