extern crate lalrpop;

fn main() {
    match lalrpop::process_root() {
        Ok(()) => (),
        Err(error) => {
            format!("{}", error);
            std::process::exit(1)
        }
    }
}
